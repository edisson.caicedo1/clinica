<div class="menu__side" id="menu_side">

        <div class="name__page">
            <img src="img/img_logo_tizz@2x.png" alt="" height="50" width="80">
        </div>

        <div class="options__menu">	

            <a href="admintrabajadores.php" class="selected">
                <div class="option">
                    <i class="ri-contacts-book-line"></i>
                    <h4>Trabajadores</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="ri-hospital-line"></i>
                    <h4>Clínicas</h4>
                </div>
            </a>
            
            <a href="adminclientespotenciales.php">
                <div class="option">
                    <i class="ri-folder-user-fill"></i>
                    <h4>Clientes potenciales</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="ri-creative-commons-by-line"></i>
                    <h4>Pacientes</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="ri-article-line"></i>
                    <h4>Agenda</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="ri-profile-fill"></i>
                    <h4>Parametrización</h4>
                </div>
            </a>

        </div>

    </div>