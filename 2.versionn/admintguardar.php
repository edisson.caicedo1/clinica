<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/img_logo_tizz.ico">
    
    <title>Clínica</title>

    <link rel="stylesheet" href="css/estilos.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'>
    <script src="https://kit.fontawesome.com/41bcea2ae3.js" crossorigin="anonymous"></script>
    
</head>
<body id="body">
    
    <header>
        <div class="contenidot">
            <div class="izquierdat">
                <div class="icon__menu">
                    <i class="fas fa-bars" id="btn_open"></i>
                </div>
            </div>
            <div class="centrot">
                <h4>Trabajadores</h4>
            </div>
            <div class="derechat">
                <p>
                    <a href="salir.php">
                        <img src="../img/salir.png" alt="">
                    </a>
                </p>
            </div>
        </div>
    </header>

    <?php include ('menu.php') ?>

    <main>
        <!--Extraer datos bd -->

<?php
  include "conexion.php";
  $sql="SELECT * FROM clinicas";
  $query = mysqli_query($mysqli, $sql);
?>

<!--Añadir -->

    <form action="guardar_admint.php" method="POST" enctype="multipart/form-data">
        <label for="clinica">Clínica</label>
        <select name="clinica" required>
            <option selected></option>
              <?php
              while ($row = mysqli_fetch_array($query))
              {
              ?>
              <option value="<?php echo $row['codclinica']; ?>"><?php echo $row['nombre']; ?></option>
              <?php
              }
              ?>
        </select>
        
        <label for="rol">Rol</label>
        <select name="rol" required>
            <option selected></option>
            <option value="Directores">Directores</option>
            <option value="Administrador">Administrador</option>
            <option value="Recepción">Recepción</option>
        </select>

        <label for="foto">Foto</label>
        <input type="file" id="foto" name="foto" accept="image/png, .jpeg, .jpg" required>
        
        <label for="nombres">Nombres</label>
        <input type="text" id="nombres" name="nombres" required>

        <label for="apellidos">Apellidos</label>
        <input type="text" id="apellidos" name="apellidos" required>

        <label for="documento">Documento</label>
        <input type="number" id="documento" name="documento" required>

        <label for="fechan">Fecha Nacimiento</label>
        <input type="date" id="fecha_nacimiento" name="fecha_nacimiento" required>
        
        <label for="direccion">Dirección</label>
        <input type="text" id="direccion" name="direccion" required>

        <label for="telefono">Teléfono</label>
        <input type="number" id="telefono" name="telefono" required>

        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required>
        
        <label for="titulo_pregrado">Título pregrado</label>
        <input type="text" id="titulo_pregrado" name="titulo_pregrado" required>
        
        <label for="tpregrado">Soporte título</label>
        <input type="file" id="tpregrado" name="tpregrado" accept="application/pdf" required>

        <label for="titulo_esp">Título especialización</label>
        <input type="text" id="titulo_esp" name="titulo_esp">
        
        <label for="tespe">Soporte título</label>
        <input type="file" id="tespe" name="tespe" accept="application/pdf">

        <label for="titulo_maestria">Título maestría</label>
        <input type="text" id="titulo_maestria" name="titulo_maestria">
        
        <label for="tespe">Soporte título</label>
        <input type="file" id="tmaestria" name="tmaestria" accept="application/pdf">

        <label for="estado">Estado</label>
        <select name="estado" required>
            <option selected></option>
            <option value="Activo">Activo</option>
            <option value="Inactivo">Inactivo</option>
        </select>

        <label for="fecha">Fecha de inscripción</label>
        <input type="date" id="fecha" name="fecha" required>
        
        <input type="submit" value="Guardar">
        <!--<a class="btn_regresar" href="admintrabajadores.php" type="button">Regresar</a>-->
      </form>

      </div>
    </div>
  </div>
</div>
    </main>

    <script src="js/script.js"></script>
</body>
</html>