<?php 
   session_start();
   $loginnombre=$_SESSION['login'];
   if($_SESSION["logueado"]==TRUE)
   {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/img_logo_tizz.ico">
    <title>Clínica</title>

    <link rel="stylesheet" href="css/estilos.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'>

    <script src="https://kit.fontawesome.com/41bcea2ae3.js" crossorigin="anonymous"></script>
</head>
<body id="body">
    
    <header>
        <div class="contenido">
            <div class="izquierda">
                <div class="icon__menu">
                    <i class="fas fa-bars" id="btn_open"></i>
                </div>
            </div>
            <div class="derecha">
                <p>
                    <a href="salir.php">
                        <img src="../img/salir.png" alt="">
                    </a>
                </p>
            </div>
        </div>
    </header>

    <?php include ('menumedico.php') ?>

    <main>
        <img class="imagen" src="img/img_backgrnd.png" alt="" width="100%">
    </main>

    <script src="js/script.js"></script>
</body>
</html>
<?php
}
else
{
    header("Location: index.html");
}
?>