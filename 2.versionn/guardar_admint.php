<?php
require_once "conexion.php";

if($_FILES["foto"])
{
    //Subir foto
    $nombrefoto = basename($_FILES["foto"]["name"]);
    $nombrefinal = date("y-m-d")."_".$nombrefoto;
    $ruta = "archivos/".$nombrefinal;
    $subirarchivo = move_uploaded_file($_FILES["foto"]["tmp_name"],$ruta);

    //Subir titulo pregrado
    $nombrepregrado = basename($_FILES["tpregrado"]["name"]);
    $nombrepregradof = date("y-m-d")."_".$nombrepregrado;
    $rutap = "archivos_estudiost/".$nombrepregradof;
    $subirarchivopre = move_uploaded_file($_FILES["tpregrado"]["tmp_name"],$rutap);

    //Subir titulo especialización
    $nombreesp = basename($_FILES["tespe"]["name"]);
    $nombreespecializacionf = date("y-m-d")."_".$nombreesp;
    $rutae = "archivos_estudiost/".$nombreespecializacionf;
    $subirarchivoesp = move_uploaded_file($_FILES["tespe"]["tmp_name"],$rutae);

    //Subir titulo maestria
    $nombremaes = basename($_FILES["tmaestria"]["name"]);
    $nombremaestriaf = date("y-m-d")."_".$nombremaes;
    $rutam = "archivos_estudiost/".$nombremaestriaf;
    $subirarchivomaes = move_uploaded_file($_FILES["tmaestria"]["tmp_name"],$rutam);

    if($subirarchivo)
    {
        if (
            isset($_POST['clinica']) && !empty($_POST['clinica']) &&
            isset($_POST['rol']) && !empty($_POST['rol']) &&
            isset($_POST['nombres']) && !empty($_POST['nombres']) &&
            isset($_POST['apellidos']) && !empty($_POST['apellidos']) &&
            isset($_POST['documento']) && !empty($_POST['documento'])&&
            isset($_POST['fecha_nacimiento']) && !empty($_POST['fecha_nacimiento'])&&
            isset($_POST['direccion']) && !empty($_POST['direccion'])&&
            isset($_POST['telefono']) && !empty($_POST['telefono'])&&
            isset($_POST['email']) && !empty($_POST['email'])&&
            isset($_POST['titulo_pregrado']) && !empty($_POST['titulo_pregrado'])&&
            isset($_POST['estado']) && !empty($_POST['estado'])&&
            isset($_POST['fecha']) && !empty($_POST['fecha'])
        ) 
        {
            $clinica = $_POST['clinica'];
            $rol = $_POST['rol'];
            $nombres = $_POST['nombres'];
            $apellidos = $_POST['apellidos'];
            $documento = $_POST['documento'];
            $fecha_nacimiento = $_POST['fecha_nacimiento'];
            $direccion = $_POST['direccion'];
            $telefono = $_POST['telefono'];
            $email = $_POST['email'];
            $titulo_pregrado = $_POST['titulo_pregrado'];
            $titulo_esp = $_POST['titulo_esp'];
            $titulo_maestria = $_POST['titulo_maestria'];
            $estado = $_POST['estado'];
            $fecha = $_POST['fecha'];
            
            if(mysqli_query($mysqli, "INSERT INTO trabajadores (codclinica, rol, foto, nombrest, apellidost, documento, fecha_nacimiento, direccion, telefono, email, titulo_pregrado, titulo_pregrado_s, titulo_esp, titulo_esp_s, titulo_maestria, titulo_maestria_s, estado, fecha_incorporacion) VALUES ('$clinica', '$rol', '$ruta', '$nombres', '$apellidos','$documento','$fecha_nacimiento','$direccion','$telefono','$email','$titulo_pregrado', '$rutap', '$titulo_esp', '$rutae', '$titulo_maestria', '$rutam', '$estado','$fecha')"))
            {
                ?>
                <!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="UTF-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                            <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                            <script src="sweetalert2.all.min.js"></script>
                        </head>
                        <body>
                        <script>
                                Swal.fire({
                                    title: 'Terminado',
                                    text: "Datos almacenados correctamente",
                                    icon: 'success',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Ok'
                                    }).then((result) => {
                                    if (result.isConfirmed) {
                                            window.location.href='admintrabajadores.php';
                                    }
                                })
                        </script>    
                        </body>
                </html>
                <?php
            }
            else
            {
                ?>
                <!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="UTF-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                            <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                            <script src="sweetalert2.all.min.js"></script>
                        </head>
                        <body>
                        <script>
                                Swal.fire({
                                    title: 'Error',
                                    text: "No fue posible tramitar tu solicitud, intenta más tarde.",
                                    icon: 'error',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Ok'
                                    }).then((result) => {
                                    if (result.isConfirmed) {
                                            window.location.href='admintrabajadores.php';
                                    }
                                })
                        </script>    
                        </body>
                </html>
                <?php
            }
        }
        
    }
}


?>