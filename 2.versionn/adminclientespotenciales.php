<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/img_logo_tizz.ico">
    <title>Clínica</title>

    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/modalb.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css">
    <script src="https://kit.fontawesome.com/41bcea2ae3.js" crossorigin="anonymous"></script>
</head>
<body id="body">
    
    <header>
        <div class="contenidot">
            <div class="izquierdat">
                <div class="icon__menu">
                    <i class="fas fa-bars" id="btn_open"></i>
                </div>
            </div>
            <div class="centrot">
                <h4>Clientes Potenciales</h4>
            </div>
            <div class="derechat">
                <p>
                    <a href="salir.php">
                        <img src="../img/salir.png" alt="">
                    </a>
                </p>
            </div>
        </div>
    </header>

    <?php include ('menu.php') ?>

    <main>
        <div class="menuc">
            <p align="right">
              <button type="button" class="boton_agregar"><a href="admintguardar.php" class="atexto">Agregar </a></button>
            </p>
        </div>
        <br>
        <table>
            <thead>
                <tr>
                <th colspan="2">Fecha y hora primer contacto</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Teléfono</th>
                <th>Medio de contacto</th>
                <th>Estado</th>
                <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    include "conexion.php";
                    $sql="SELECT * FROM clientespotenciales";
                    $query = mysqli_query($mysqli, $sql);
                    while ($row = mysqli_fetch_array($query))
                    {?>
                <tr>
                    <td><?php echo $row['fecha_contacto']; ?></td>
                    <td><?php echo $row['hora_contacto']; ?></td>
                    <td><?php echo $row['nombresc']; ?></td>
                    <td><?php echo $row['apellidosc']; ?></td>
                    <td><?php echo $row['telefono']; ?></td>
                    <td>
                        <button type="button" data-bs-toggle="modal" data-bs-target="#mensajescorreo" data-bs-whatever="@mdo" value="<?php echo $row['codclientespotenciales']?>" name="id" id="id">g</button>
                        <img src="img/ima_email.svg" alt="" type="button" data-bs-toggle="modal" data-bs-target="#mensajescorreo" data-bs-whatever="@mdo">
                        <img src="img/ima_wp.svg" alt="" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@mdo">
                        <img src="img/ima_tel.svg" alt="" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@mdo">
                        <img src="img/ima_llam.svg" alt="" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@mdo">
                        <img src="img/mas.svg" alt="" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@mdo">
                    </td>
                    <td><?php echo $row['estado']; ?></td>
                    <!--
                    <td>
                        <button class="boton_editar" href="otra_pagina.html"><a href="adminclientespotencialesmas.php?id=<?php echo $row['codclientespotenciales']?>" class="atexto">Más</a></button>
                        
                        <button class="boton_editar" href="otra_pagina.html"><a href="admintrabajadoresedit.php?id=<?php echo $row['codclientespotenciales']?>" class="atexto">Editar</a></button>
                        
                    </td>
                    -->
                </tr>
                <?php
                    }                    
                ?>
            </tbody>
        </table>
    </main>
<!--Modal-->

<?php
    if (isset($_GET['id'])) 
    {
        $valor = $_GET['id'];
    } 
    else 
    {
        $valor = "N";
    }
?>
<div class="modal fade" id="mensajescorreo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title fs-5" id="exampleModalLabel">Mensajería Correo</h2>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="enviarcorreos.php" method="POST">
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Destino:</label>
            <input type="email" class="form-control" id="recipient-name" name="destino" required>
          </div>
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Asunto:</label>
            <input type="text" class="form-control" id="message-text" name="asunto" required>
          </div>
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Mensaje:</label>
            <textarea class="form-control" id="message-text" name="mensaje"></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Send message</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!--Fin modal -->



    <script src="js/script.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>
</html>