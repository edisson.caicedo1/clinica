<div class="menu__side" id="menu_side">

        <div class="name__page">
            <img src="img/img_logo_tizz@2x.png" alt="" height="50" width="80">
        </div>

        <div class="options__menu">	

            <a href="medicoagenda.php" class="selected">
                <div class="option">
                    <i class="ri-article-line"></i>
                    <h4>Agenda</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="ri-creative-commons-by-line"></i>
                    <h4>Pacientes</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="ri-profile-fill"></i>
                    <h4>Parametrización</h4>
                </div>
            </a>

        </div>

    </div>