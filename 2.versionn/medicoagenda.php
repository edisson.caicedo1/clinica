<?php 
   session_start();
   $loginnombre=$_SESSION['login'];
   if($_SESSION["logueado"]==TRUE)
   {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/img_logo_tizz.ico">
    <title>Clínica</title>

    <link rel="stylesheet" href="css/estilos.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'>

    <script src="https://kit.fontawesome.com/41bcea2ae3.js" crossorigin="anonymous"></script>
</head>
<body id="body">
    
    <header>
        <div class="contenido">
            <div class="izquierda">
                <div class="icon__menu">
                    <i class="fas fa-bars" id="btn_open"></i>
                </div>
            </div>
            <div class="derecha">
                <p>
                    <a href="salir.php">
                        <img src="../img/salir.png" alt="">
                    </a>
                </p>
            </div>
        </div>
    </header>

    <?php include ('menumedico.php') ?>

    <main>
        <h4 align="center">Febrero</h4>
    <table class="calendar">
        <thead>
            <tr>
                <th colspan="7" class="month-year"></th>
            </tr>
            <tr>
                <th>Dom</th>
                <th>Lun</th>
                <th>Mar</th>
                <th>Mié</th>
                <th>Jue</th>
                <th>Vie</th>
                <th>Sáb</th>
            </tr>
        </thead>
        <tbody class="days">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
            <tr>
                <td>5</td>
                <td>7</td>
                <td>7</td>
                <td>8</td>
                <td>9</td>
                <td>10</td>
                <td>11</td>
            </tr>
            <tr>
                <td>12</td>
                <td>13</td>
                <td>14</td>
                <td>15</td>
                <td>16</td>
                <td>17</td>
                <td>18</td>
            </tr>
            <tr>
                <td>19</td>
                <td>20</td>
                <td>21</td>
                <td>22</td>
                <td>23</td>
                <td>24</td>
                <td>25</td>
            </tr>
            <tr>
                <td>26</td>
                <td>27</td>
                <td>28</td>
            </tr>
        </tbody>
    </table>


    </main>

    <script src="js/script.js"></script>
</body>
</html>
<?php
}
else
{
    header("Location: index.html");
}
?>