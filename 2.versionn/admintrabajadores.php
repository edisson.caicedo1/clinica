<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/img_logo_tizz.ico">
    <title>Clínica</title>

    <link rel="stylesheet" href="css/estilos.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'>
    <script src="https://kit.fontawesome.com/41bcea2ae3.js" crossorigin="anonymous"></script>
    
</head>
<body id="body">
    
    <header>
        <div class="contenidot">
            <div class="izquierdat">
                <div class="icon__menu">
                    <i class="fas fa-bars" id="btn_open"></i>
                </div>
            </div>
            <div class="centrot">
                <h4>Trabajadores</h4>
            </div>
            <div class="derechat">
                <p>
                    <a href="salir.php">
                        <img src="../img/salir.png" alt="">
                    </a>
                </p>
            </div>
        </div>
    </header>

    <?php include ('menu.php') ?>

    <main>
        <div class="menuc">
            <p align="right">
              <button type="button" class="boton_agregar"><a href="admintguardar.php" class="atexto">Agregar </a></button>
            </p>
        </div>
        <br>
        <table>
            <thead>
                <tr>
                <th>Foto</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Documento</th>
                <th>Estado</th>
                <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    include "conexion.php";
                    $sql="SELECT * FROM trabajadores";
                    $query = mysqli_query($mysqli, $sql);
                    while ($row = mysqli_fetch_array($query))
                    {?>
                <tr>
                    <td><img class="img-circle" src="<?php echo $row['foto']; ?>" width="50" height="50"></td>
                    <td><?php echo $row['nombrest']; ?></td>
                    <td><?php echo $row['apellidost']; ?></td>
                    <td><?php echo $row['documento']; ?></td>
                    <td><?php echo $row['estado']; ?></td>
                    <td>
                        <button class="boton_editar" href="otra_pagina.html"><a href="admintrabajadoresmas.php?id=<?php echo $row['codtrabajador']?>" class="atexto">Más</a></button>
                        
                        <button class="boton_editar" href="otra_pagina.html"><a href="admintrabajadoresedit.php?id=<?php echo $row['codtrabajador']?>" class="atexto">Editar</a></button>
                        
                    </td>
                </tr>
                <?php
                    }                    
                ?>
            </tbody>
        </table>
    </main>

    <script src="js/script.js"></script>
</body>
</html>