<?php 
   session_start();
   $loginnombre=$_SESSION['login'];
   if($_SESSION["logueado"]==TRUE)
   {

?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Clínica</title>
  <link rel='stylesheet' href='https://unpkg.com/css-pro-layout@1.1.0/dist/css/css-pro-layout.css'>
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'><link rel="stylesheet" href="css/style.css">
</head>
<body>
<!-- partial:index.partial.html -->

    <?php include ('menu.php') ?>

  <div id="overlay" class="overlay"></div>
  <div class="layout">
    <header class="header">
      <a id="btn-collapse" href="#">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      
      <a id="btn-toggle" href="#" class="sidebar-toggler break-point-lg">
        <i class="ri-menu-line ri-xl"></i>
        </a>
    </header>
    <main class="content">
      <div>
        <img class="img-fluid" src="img/dentista.jpg" style="width: 100%; height: auto;" />
        
      </div>
      
      <footer class="footer">
        <small style="margin-bottom: 20px; display: inline-block">
          © 2022
        </small>
        <br />
        
      </footer>
    </main>
    <div class="overlay"></div>
  </div>
</div>
<!-- partial -->
  <script src='https://unpkg.com/@popperjs/core@2'></script><script  src="./script.js"></script>

</body>
</html>
<?php
}


else
{
    header("Location: index.html");
}
?>