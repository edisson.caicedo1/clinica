<?php
require_once "conexion.php";

if (
    isset($_POST['cod']) && !empty($_POST['cod']) &&
	isset($_POST['nombre']) && !empty($_POST['nombre']) &&
	isset($_POST['precio']) && !empty($_POST['precio']) &&
	isset($_POST['estado']) && !empty($_POST['estado'])
) 
{
    $cod = $_POST['cod'];
    $nombre = $_POST['nombre'];
	$precio = $_POST['precio'];
    $estado = $_POST['estado'];
    if(mysqli_query($mysqli, "INSERT INTO gabinete (nombre, precio, estado, codclinica) VALUES ('$nombre', '$precio', '$estado', '$cod')"))
    {
        ?>
        <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                    <script src="sweetalert2.all.min.js"></script>
                </head>
                <body>
                <script>
                        Swal.fire({
                            title: 'Terminado',
                            text: "Datos almacenados correctamente",
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                    window.location.href='adminclinicas.php';
                            }
                        })
                </script>    
                </body>
        </html>
        <?php
    }
    else
    {
        ?>
        <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                    <script src="sweetalert2.all.min.js"></script>
                </head>
                <body>
                <script>
                        Swal.fire({
                            title: 'Error',
                            text: "No fue posible tramitar tu solicitud, intenta más tarde.",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href='adminclinicas.php';
                            }
                        })
                </script>    
                </body>
        </html>
        <?php
    }
}

?>