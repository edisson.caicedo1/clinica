<!--Extraer datos bd -->

<?php
  include "conexion.php";
  $sql="SELECT * FROM clinicas";
  $query = mysqli_query($mysqli, $sql);
?>

<!--Dat toogle Añadir -->

<div class="modal fade" id="guardarPaciente" tabindex="-1" aria-labelledby="guardarPaciente" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Pacientes</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form action="guardar_adminp.php" method="POST" enctype="multipart/form-data">
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Clínica</label>
            <select class="form-select" aria-label="Default select example" name="clinica" required>
            <option selected></option>
              <?php
              while ($row = mysqli_fetch_array($query))
              {
              ?>
              <option value="<?php echo $row['codclinica']; ?>"><?php echo $row['nombre']; ?></option>
              <?php
              }
              ?>
            </select>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Foto</label>
            <input type="file" class="form-control" id="foto" name="foto" accept="image/png, .jpeg, .jpg" required>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">DNI</label>
            <input type="text" class="form-control" id="dni" name="dni" placeholder="DNI">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Nombres</label>
            <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Apellidos</label>
            <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">E-mail</label>
            <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo electrónico">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Teléfono</label>
            <input type="number" class="form-control" id="telefono" name="telefono" placeholder="telefono">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Pago pendiente</label>
            <input type="text" class="form-control" id="pagop" name="pagop" placeholder="Pago pendiente">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Etiqueta</label>
            <select class="form-select" aria-label="Default select example" name="etiqueta">
              <option selected></option>
              <option value="Bueno">Bueno</option>
              <option value="Regular">Regular</option>
              <option value="Malo">Malo</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Estado</label>
            <select class="form-select" aria-label="Default select example" name="estado">
              <option selected></option>
              <option value="Citado">Citado</option>
              <option value="Vino y no firmó">Vino y no firmó</option>
              <option value="No vino y aviso">No vino y aviso</option>
              <option value="No acude">No acude</option>
              <option value="Pendiente cita">Pendiente cita</option>
              <option value="En tratamiento">En tratamiento</option>
              <option value="Finalizado">Finalizado</option>
            </select>
          </div>
            <button type="submit" class="btn btn-primary">Guardar</button>  
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        </form>
      </div>
    </div>
  </div>
</div>