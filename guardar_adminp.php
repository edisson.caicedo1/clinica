<?php
require_once "conexion.php";

if($_FILES["foto"])
{
    //Subir foto
    $nombrefoto = basename($_FILES["foto"]["name"]);
    $nombrefinal = date("y-m-d")."_".$nombrefoto;
    $ruta = "archivos_pacientes/".$nombrefinal;
    $subirarchivo = move_uploaded_file($_FILES["foto"]["tmp_name"],$ruta);
    if($subirarchivo)
    {
        if (
            isset($_POST['clinica']) && !empty($_POST['clinica']) &&
            isset($_POST['dni']) && !empty($_POST['dni']) &&
            isset($_POST['nombres']) && !empty($_POST['nombres'])&&
            isset($_POST['apellidos']) && !empty($_POST['apellidos'])&&
            isset($_POST['correo']) && !empty($_POST['correo'])&&
            isset($_POST['telefono']) && !empty($_POST['telefono'])&&
            isset($_POST['pagop']) && !empty($_POST['pagop'])&&
            isset($_POST['etiqueta']) && !empty($_POST['etiqueta'])&&
            isset($_POST['estado']) && !empty($_POST['estado'])
        ) 
        {
            $codcl = $_POST['clinica'];
            $dni = $_POST['dni'];
            $nombres = $_POST['nombres'];
            $apellidos = $_POST['apellidos'];
            $correo = $_POST['correo'];
            $telefono = $_POST['telefono'];
            $pagop = $_POST['pagop'];
            $etiqueta = $_POST['etiqueta'];
            $estado = $_POST['estado'];
            if(mysqli_query($mysqli, "INSERT INTO paciente (codpaciente, codclinica, foto, nombresp, apellidosp, correo, telefono, pagopendiente, etiqueta, estado) VALUES ('$dni', '$codcl', '$ruta', '$nombres', '$apellidos','$correo','$telefono','$pagop','$etiqueta','$estado')"))
            {
                ?>
                <!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="UTF-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                            <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                            <script src="sweetalert2.all.min.js"></script>
                        </head>
                        <body>
                        <script>
                                Swal.fire({
                                    title: 'Terminado',
                                    text: "Datos almacenados correctamente",
                                    icon: 'success',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Ok'
                                    }).then((result) => {
                                    if (result.isConfirmed) {
                                            window.location.href='adminPacientes.php';
                                    }
                                })
                        </script>    
                        </body>
                </html>
                <?php
            }
            else
            {
                ?>
                <!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="UTF-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                            <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                            <script src="sweetalert2.all.min.js"></script>
                        </head>
                        <body>
                        <script>
                                Swal.fire({
                                    title: 'Error',
                                    text: "No fue posible tramitar tu solicitud, intenta más tarde.",
                                    icon: 'error',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Ok'
                                    }).then((result) => {
                                    if (result.isConfirmed) {
                                            window.location.href='adminPacientes.php';
                                    }
                                })
                        </script>    
                        </body>
                </html>
                <?php
            }
        }
        
    }
}


?>