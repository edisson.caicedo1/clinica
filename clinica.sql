-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-01-2023 a las 12:15:34
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `clinica`
--
CREATE DATABASE IF NOT EXISTS `clinica` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `clinica`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cclientespotenciales`
--

DROP TABLE IF EXISTS `cclientespotenciales`;
CREATE TABLE `cclientespotenciales` (
  `codcclientespotenciales` int(11) NOT NULL,
  `codclientespotenciales` int(11) DEFAULT NULL,
  `fecha_contacto` varchar(100) DEFAULT NULL,
  `medio` varchar(100) DEFAULT NULL,
  `observaciones` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cclientespotenciales`
--

INSERT INTO `cclientespotenciales` (`codcclientespotenciales`, `codclientespotenciales`, `fecha_contacto`, `medio`, `observaciones`) VALUES
(1, 3, '2023-01-11', 'Llamada', 'Llamar despues');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

DROP TABLE IF EXISTS `citas`;
CREATE TABLE `citas` (
  `codcitas` int(11) NOT NULL,
  `codpaciente` bigint(20) DEFAULT NULL,
  `codtrabajador` int(11) DEFAULT NULL,
  `fecha_cita` varchar(70) DEFAULT NULL,
  `hora_cita` varchar(70) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`codcitas`, `codpaciente`, `codtrabajador`, `fecha_cita`, `hora_cita`, `estado`) VALUES
(3, 321, 3, '2023-01-12', '15:45', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientespotenciales`
--

DROP TABLE IF EXISTS `clientespotenciales`;
CREATE TABLE `clientespotenciales` (
  `codclientespotenciales` int(11) NOT NULL,
  `codclinica` int(11) DEFAULT NULL,
  `fecha_contacto` varchar(100) DEFAULT NULL,
  `hora_contacto` varchar(100) DEFAULT NULL,
  `nombresc` varchar(100) DEFAULT NULL,
  `apellidosc` varchar(100) DEFAULT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `wap` varchar(50) DEFAULT NULL,
  `cor` varchar(30) DEFAULT NULL,
  `txt` varchar(30) DEFAULT NULL,
  `llamada` varchar(30) DEFAULT NULL,
  `estado` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `clientespotenciales`
--

INSERT INTO `clientespotenciales` (`codclientespotenciales`, `codclinica`, `fecha_contacto`, `hora_contacto`, `nombresc`, `apellidosc`, `telefono`, `correo`, `wap`, `cor`, `txt`, `llamada`, `estado`) VALUES
(3, 4, '2023-01-10', '10:00', 'Prueba', 'Prueba Prueba', '357', 'prueba@prueba.com', 'Si', 'Si', 'Si', 'Si', 'Citado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clinicas`
--

DROP TABLE IF EXISTS `clinicas`;
CREATE TABLE `clinicas` (
  `codclinica` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `clinicas`
--

INSERT INTO `clinicas` (`codclinica`, `nombre`, `direccion`, `estado`) VALUES
(4, 'Norte', 'Calle 18 No. 10-09', 'Activo'),
(5, 'Principal', 'Cra 7 No. 98-27', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadisticas_t`
--

DROP TABLE IF EXISTS `estadisticas_t`;
CREATE TABLE `estadisticas_t` (
  `codestadisticas_t` int(11) NOT NULL,
  `codtrabajador` int(11) DEFAULT NULL,
  `pacientes_atendidos` int(11) DEFAULT NULL,
  `pacientes_recomiendan` int(11) DEFAULT NULL,
  `citas_atendidas` int(11) DEFAULT NULL,
  `indice` int(11) DEFAULT NULL,
  `tiempo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gabinete`
--

DROP TABLE IF EXISTS `gabinete`;
CREATE TABLE `gabinete` (
  `codgabinete` int(11) NOT NULL,
  `nombre` varchar(70) DEFAULT NULL,
  `precio` varchar(60) DEFAULT NULL,
  `estado` varchar(100) DEFAULT NULL,
  `codclinica` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `gabinete`
--

INSERT INTO `gabinete` (`codgabinete`, `nombre`, `precio`, `estado`, `codclinica`) VALUES
(1, 'Prueba', '123123', 'Activo', 4),
(2, 'Prueba', '9879', 'Activo', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

DROP TABLE IF EXISTS `paciente`;
CREATE TABLE `paciente` (
  `codpaciente` bigint(20) NOT NULL,
  `codclinica` int(11) DEFAULT NULL,
  `foto` varchar(200) NOT NULL,
  `nombresp` varchar(50) DEFAULT NULL,
  `apellidosp` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `pagopendiente` varchar(80) DEFAULT NULL,
  `etiqueta` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `peso` varchar(50) NOT NULL,
  `altura` varchar(50) NOT NULL,
  `alergias` varchar(200) NOT NULL,
  `enfermedades` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`codpaciente`, `codclinica`, `foto`, `nombresp`, `apellidosp`, `correo`, `telefono`, `pagopendiente`, `etiqueta`, `estado`, `peso`, `altura`, `alergias`, `enfermedades`) VALUES
(321, 4, 'archivos_pacientes/23-01-12_Edisson.jpg', 'Prueba', 'Prueba Prueba', 'prueba@prueba.com', 321456, '123', 'Bueno', 'Vino y no firmó', '56', '1.65', 'Ninguna', 'Ninguna');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajadores`
--

DROP TABLE IF EXISTS `trabajadores`;
CREATE TABLE `trabajadores` (
  `codtrabajador` int(11) NOT NULL,
  `codclinica` int(11) DEFAULT NULL,
  `rol` varchar(70) DEFAULT NULL,
  `foto` varchar(80) DEFAULT NULL,
  `nombrest` varchar(100) DEFAULT NULL,
  `apellidost` varchar(100) DEFAULT NULL,
  `documento` varchar(100) DEFAULT NULL,
  `fecha_nacimiento` varchar(100) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `titulo_pregrado` varchar(100) DEFAULT NULL,
  `titulo_pregrado_s` varchar(40) DEFAULT NULL,
  `titulo_esp` varchar(100) DEFAULT NULL,
  `titulo_esp_s` varchar(40) DEFAULT NULL,
  `titulo_maestria` varchar(100) DEFAULT NULL,
  `titulo_maestria_s` varchar(40) DEFAULT NULL,
  `estado` varchar(70) DEFAULT NULL,
  `fecha_incorporacion` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `trabajadores`
--

INSERT INTO `trabajadores` (`codtrabajador`, `codclinica`, `rol`, `foto`, `nombrest`, `apellidost`, `documento`, `fecha_nacimiento`, `direccion`, `telefono`, `email`, `titulo_pregrado`, `titulo_pregrado_s`, `titulo_esp`, `titulo_esp_s`, `titulo_maestria`, `titulo_maestria_s`, `estado`, `fecha_incorporacion`) VALUES
(3, 4, 'Directores', 'archivos/23-01-12_Edisson.jpg', 'Edisson', 'Caicedo', '1049608278', '1987-07-19', 'Calle 18 No. 10-09', '3175768681', 'edisson.caicedo1@gmail.com', 'Ingeniero de Sistemas', 'archivos_estudiost/23-01-12_Cuenta de co', 'Bases de datos', 'archivos_estudiost/23-01-12_Edisson.pdf', 'Ingeniería del Software', 'archivos_estudiost/23-01-12_img032.pdf', 'Activo', '2023-01-11'),
(4, 5, 'Administrador', 'archivos/23-01-14_Imagen1.jpg', 'Prueba', 'Prueba', '33333333', '1985-07-19', 'Prueba', '333333333', 'prueba@prueba.com', 'prueba', 'archivos_estudiost/23-01-14_img034.pdf', '', 'archivos_estudiost/23-01-14_', '', 'archivos_estudiost/23-01-14_', 'Activo', '2023-01-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tratamiento`
--

DROP TABLE IF EXISTS `tratamiento`;
CREATE TABLE `tratamiento` (
  `codtratamiento` int(11) NOT NULL,
  `codpaciente` bigint(20) DEFAULT NULL,
  `fecha_cita` varchar(150) NOT NULL,
  `hora_cita` varchar(100) NOT NULL,
  `tratamiento` varchar(200) NOT NULL,
  `quien_apunta` varchar(200) NOT NULL,
  `fecha_registro` varchar(150) NOT NULL,
  `hora_registro` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `pago` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `tratamiento`
--

INSERT INTO `tratamiento` (`codtratamiento`, `codpaciente`, `fecha_cita`, `hora_cita`, `tratamiento`, `quien_apunta`, `fecha_registro`, `hora_registro`, `estado`, `pago`) VALUES
(1, 321, '2023-01-12', '15:15', 'Control general', 'Edisson Caicedo', '2023-01-12', '15:16', 'Completada', 'Pagó');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `codusuario` int(11) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `rol` varchar(50) DEFAULT NULL,
  `estado` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`codusuario`, `password`, `rol`, `estado`) VALUES
(123, '123', 'Administrador', 'Activo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cclientespotenciales`
--
ALTER TABLE `cclientespotenciales`
  ADD PRIMARY KEY (`codcclientespotenciales`),
  ADD KEY `codclientespotenciales` (`codclientespotenciales`);

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`codcitas`),
  ADD KEY `codpaciente` (`codpaciente`),
  ADD KEY `codtrabajador` (`codtrabajador`);

--
-- Indices de la tabla `clientespotenciales`
--
ALTER TABLE `clientespotenciales`
  ADD PRIMARY KEY (`codclientespotenciales`),
  ADD KEY `codclinica` (`codclinica`);

--
-- Indices de la tabla `clinicas`
--
ALTER TABLE `clinicas`
  ADD PRIMARY KEY (`codclinica`);

--
-- Indices de la tabla `estadisticas_t`
--
ALTER TABLE `estadisticas_t`
  ADD PRIMARY KEY (`codestadisticas_t`),
  ADD KEY `codtrabajador` (`codtrabajador`);

--
-- Indices de la tabla `gabinete`
--
ALTER TABLE `gabinete`
  ADD PRIMARY KEY (`codgabinete`),
  ADD KEY `codclinica` (`codclinica`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`codpaciente`),
  ADD KEY `codclinica` (`codclinica`);

--
-- Indices de la tabla `trabajadores`
--
ALTER TABLE `trabajadores`
  ADD PRIMARY KEY (`codtrabajador`),
  ADD KEY `codclinica` (`codclinica`);

--
-- Indices de la tabla `tratamiento`
--
ALTER TABLE `tratamiento`
  ADD PRIMARY KEY (`codtratamiento`),
  ADD KEY `codpaciente` (`codpaciente`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`codusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cclientespotenciales`
--
ALTER TABLE `cclientespotenciales`
  MODIFY `codcclientespotenciales` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `codcitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `clientespotenciales`
--
ALTER TABLE `clientespotenciales`
  MODIFY `codclientespotenciales` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `clinicas`
--
ALTER TABLE `clinicas`
  MODIFY `codclinica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `estadisticas_t`
--
ALTER TABLE `estadisticas_t`
  MODIFY `codestadisticas_t` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gabinete`
--
ALTER TABLE `gabinete`
  MODIFY `codgabinete` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `trabajadores`
--
ALTER TABLE `trabajadores`
  MODIFY `codtrabajador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tratamiento`
--
ALTER TABLE `tratamiento`
  MODIFY `codtratamiento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cclientespotenciales`
--
ALTER TABLE `cclientespotenciales`
  ADD CONSTRAINT `cclientespotenciales_ibfk_1` FOREIGN KEY (`codclientespotenciales`) REFERENCES `clientespotenciales` (`codclientespotenciales`);

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `citas_ibfk_1` FOREIGN KEY (`codpaciente`) REFERENCES `paciente` (`codpaciente`),
  ADD CONSTRAINT `citas_ibfk_2` FOREIGN KEY (`codtrabajador`) REFERENCES `trabajadores` (`codtrabajador`);

--
-- Filtros para la tabla `clientespotenciales`
--
ALTER TABLE `clientespotenciales`
  ADD CONSTRAINT `clientespotenciales_ibfk_1` FOREIGN KEY (`codclinica`) REFERENCES `clinicas` (`codclinica`);

--
-- Filtros para la tabla `estadisticas_t`
--
ALTER TABLE `estadisticas_t`
  ADD CONSTRAINT `estadisticas_t_ibfk_1` FOREIGN KEY (`codtrabajador`) REFERENCES `trabajadores` (`codtrabajador`);

--
-- Filtros para la tabla `gabinete`
--
ALTER TABLE `gabinete`
  ADD CONSTRAINT `gabinete_ibfk_1` FOREIGN KEY (`codclinica`) REFERENCES `clinicas` (`codclinica`);

--
-- Filtros para la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD CONSTRAINT `paciente_ibfk_1` FOREIGN KEY (`codclinica`) REFERENCES `clinicas` (`codclinica`);

--
-- Filtros para la tabla `trabajadores`
--
ALTER TABLE `trabajadores`
  ADD CONSTRAINT `trabajadores_ibfk_1` FOREIGN KEY (`codclinica`) REFERENCES `clinicas` (`codclinica`);

--
-- Filtros para la tabla `tratamiento`
--
ALTER TABLE `tratamiento`
  ADD CONSTRAINT `tratamiento_ibfk_1` FOREIGN KEY (`codpaciente`) REFERENCES `paciente` (`codpaciente`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
