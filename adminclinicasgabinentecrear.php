<?php 
   session_start();
   $loginnombre=$_SESSION['login'];
   if($_SESSION["logueado"]==TRUE)
   {
?>

<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Clínica</title>
  <link rel='stylesheet' href='https://unpkg.com/css-pro-layout@1.1.0/dist/css/css-pro-layout.css'>
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'><link rel="stylesheet" href="css/style.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
</head>
<body>
<!-- partial:index.partial.html -->

<?php include ('menu.php'); 
$filtro = $_GET['consulta'];
?>

  <div id="overlay" class="overlay"></div>
  <div class="layout">
    <header class="header">
      <a id="btn-collapse" href="#">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <a id="btn-toggle" href="#" class="sidebar-toggler break-point-lg">
        <i class="ri-menu-line ri-xl"></i>
        </a>
        <span class="menu-title">Gabinete</span>
    </header>
    <main class="content">
      <div>
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <br>
            <form action="guardargabinete.php" method="POST">
              <input id="cod" name="cod" type="hidden" value="<?php echo $filtro; ?>">
              <div class="form-group" align="center">
                <label class="form-label" for="nombre">Gabinete</label>
                <input type="text" name="nombre" class="form-control" placeholder="Nombre Gabiente" required />
              </div>
              <div class="form-group" align="center">
                <label class="form-label" for="precio">Precio</label>
                <input type="number" name="precio" class="form-control" placeholder="Precio" required />
              </div>
              <div class="form-group" align="center">
                <label class="form-label" for="estado">Estado</label>
                <select class="form-select" aria-label="Default select example" name="estado" required>
                  <option selected></option>
                  <option value="Activo">Activo</option>
                  <option value="Inactivo">Inactivo</option>
                </select>
              </div>
              <div class="form-group" align="center">
                <button class="btn btn-success" type="submit">Guardar</button>
                <a href="adminclinicas.php" class="btn btn-warning">Atras</a>
              </div>
            </form>
          </div>
          <div class="col-md-3"></div>
        </div>
          <br>
          <div>
          <table class="table table-hover">
            <thead>
              <tr class="table-secondary">
                <th scope="col">Gabinete</th>
                <th scope="col">Precio</th>
                <th scope="col">Estado</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <?php
                    include "conexion.php";
                    
                    $sql="SELECT * FROM gabinete WHERE codclinica=$filtro";
                    $query = mysqli_query($mysqli, $sql);
                    while ($row = mysqli_fetch_array($query))
                    {?>
              <tr>
                <td><?php echo $row['nombre']; ?></td>
                <td><?php echo $row['precio']; ?></td>
                <td><?php echo $row['estado']; ?></td>
              </tr>
              <tr>
            <?php
            }                    
            ?>
            </tbody>
          </table>
        </div>
      </div>
      
      <footer class="footer">
        <small style="margin-bottom: 20px; display: inline-block">
          © 2022
        </small>
        <br />
        <div>
          
        </div>
      </footer>
    </main>
    <div class="overlay"></div>
  </div>
</div>

<!--Dat toogle Añadir -->

<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Ingresar Médico</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="administrador_medicos_crear.php" method="post">
                        <div class="form-group">
                            <label>Identificación</label>
                            <input type="number" id="identificacion" name="identificacion" class="form-control">

                        </div>
                        <div class="form-group">
                            <label>Nombres</label>
                            <input type="text" id="nombres" name="nombres" class="form-control">

                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" id="apellidos" name="apellidos" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>password</label>
                            <input type="password" id="pass" name="pass" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>E-mail</label>
                            <input type="email" id="email" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="number" id="telefono" name="telefono" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Expecialidad</label>
                            <select name="especialidad" id="especialidad" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
                              <option value=""></option>
                              <option value="Odontologo General">Odontologo General</option>
                              <option value="Ortodoncista">Ortodoncista</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Estado</label>
                            <select name="estado" id="estado" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
                              <option value=""></option>
                              <option value="Activo">Activo</option>
                              <option value="Inactivo">Inactivo</option>
                            </select>
                        </div>
                        <br><br>
                        <input type="submit" class="btn btn-primary" value="Agregar">
                        <a href="index.php" class="btn btn-default">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>


<!-- partial -->
<?php include 'admincguardar.php'; ?>
<!-- partial -->
  <script src='https://unpkg.com/@popperjs/core@2'></script><script  src="./script.js"></script>
  <!-- JavaScript Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>
</html>
<?php
}

else
{
    header("Location: index.html");
}
?>