<?php 
   session_start();
   $loginnombre=$_SESSION['login'];
   if($_SESSION["logueado"]==TRUE)
   {

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Clínica</title>
  <link rel='stylesheet' href='https://unpkg.com/css-pro-layout@1.1.0/dist/css/css-pro-layout.css'>
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'>
  <link rel="stylesheet" href="css/style.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.6.1.min.js"
    integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
</head>

<body>
  <!-- partial:index.partial.html -->

  <?php include ('menu.php') ?>

  <div id="overlay" class="overlay"></div>
  <div class="layout">
    <header class="header">
      <a id="btn-collapse" href="#">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <a id="btn-toggle" href="#" class="sidebar-toggler break-point-lg">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <span class="menu-title">Clientes Potenciales</span>
    </header>
    <main class="content">
      <div>
        <div class="row">
          <div class="col-md-12">
            <!--Tabs -->
            <ul class="nav nav-tabs mt-3">
              <li class="nav-item">
                <a href="#seccion1" class="nav-link active" data-bs-toggle="tab">Información Inicial</a>
              </li>
              <li class="nav-item">
                <a href="#seccion2" class="nav-link" data-bs-toggle="tab">Último contacto</a>
              </li>
              <li class="nav-item">
                <a href="#seccion3" class="nav-link" data-bs-toggle="tab">Acciones</a>
              </li>
            </ul>
            <!--End Tabs-->
            <!--Tabs content-->
            <div class="tab-content">
              <div class="tab-pane fade show active" id="seccion1">
                <!--Consulta a la base de datos -->
                <?php
                      include "conexion.php";
                      $id= $_GET['id'];
                      $sql="SELECT * from clientespotenciales WHERE codclientespotenciales = $id";
                      $query = mysqli_query($mysqli, $sql);

                      //$datohoy = date("Y-m-d");
                      //$datohoy = date("Y-m-d\TH-i");

                      while ($row = mysqli_fetch_array($query))
                      { 
                        $fecha_contacto = $row['fecha_contacto'];
                        $hora_contacto = $row['hora_contacto'];
                        $nombres = $row['nombresc'];
                        $apellidos = $row['apellidosc'];
                        $telefono = $row['telefono'];
                        $correo = $row['correo'];
                        $wap = $row['wap'];
                        $cor = $row['cor'];
                        $txt = $row['txt'];
                        $llamada = $row['llamada'];
                        $estado= $row['estado'];
                      }
                      $sql1="SELECT MAX(codcclientespotenciales), fecha_contacto, medio, observaciones FROM cclientespotenciales WHERE codclientespotenciales= $id";
                      $query1 = mysqli_query($mysqli, $sql1);
                      while ($row1 = mysqli_fetch_array($query1))
                      { 
                        $fecha_nuevo = $row1['fecha_contacto'];
                        $medio = $row1['medio'];
                        $observaciones = $row1['observaciones'];
                      }
                      ?>
                <!--Fin consulta a la base de datos -->
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-body">
                        <h3 align="center">Datos Cliente Potencial</h3>
                        <table class="table table">
                          <br>
                          <tr>
                            <th>Fecha contacto inicial</th>
                            <td><?php echo $fecha_contacto; ?></td>
                          </tr>
                          <tr>
                            <th>Hora contacto inicial</th>
                            <td><?php echo $hora_contacto; ?></td>
                          </tr>
                          <tr>
                            <th>Nombres</th>
                            <td><?php echo $nombres; ?></td>
                          </tr>
                          <tr>
                            <th>Apellidos</th>
                            <td><?php echo $apellidos; ?></td>
                          </tr>
                          <tr>
                            <th>Teléfono</th>
                            <td><?php echo $telefono; ?></td>
                          </tr>
                          <tr>
                            <th>Correo</th>
                            <td><?php echo $correo; ?></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>

              <div class="tab-pane fade" id="seccion2">
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-body">
                        <h3 align="center">Último contacto</h3>
                        <table class="table table">
                        <br>
                          <tr>
                          <th>Fecha último contacto</th>
                            <td><?php echo $fecha_nuevo; ?></td>
                          </tr>
                          <tr>
                          <th>Medio de contácto</th>
                            <td><?php echo $medio; ?></td>
                          </tr>
                          <tr>
                          <th>Observaciones</th>
                            <td><?php echo $observaciones; ?></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>

              <div class="tab-pane fade" id="seccion3">
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                      
                      <div class="card-body">
                        <h3 align="center">Acciones</h3>
                        <table class="table table">
                          <form action="g_adminclientespotencialesmas.php" method="POST">
                          <br>
                          <tr>
                            <input type="hidden" id="codcp" name="codcp" class="form-control" value="<?php echo $id; ?>">
                            <th>Fecha</th>
                            <td><input type="date" class="form-control" id="datoh" name="datoh" required></td>
                          </tr>
                          <tr>
                            <th>Medio de contácto</th>
                            <td>
                            <select class="form-select" aria-label="Default select example" name="medio" required>
                              <option selected></option>
                              <option value="WhastApp">WhastApp</option>
                              <option value="Correo">Correo</option>
                              <option value="Texto">Mensaje de texto</option>
                              <option value="Llamada">Llamada telefónica</option>
                            </select>
                          </tr>
                          <tr>
                            <th>Observaciones</th>
                            <td><input type="text" class="form-control" id="observaciones" name="observaciones" placeholder="Observaciones" required></td>
                          </tr>
                          <tr>
                            <td colspan="2"><center><button type="submit" class="btn btn-primary">Guardar</button></center></td>
                          </tr>
                          </form>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>

            </div>
            <!--End Tabs content-->
          </div>
        </div>
      </div>
    </main>
    <footer class="footer">
      <small style="margin-bottom: 20px; display: inline-block">
        © 2022
      </small>
      <br />
      <div>

      </div>
    </footer>
    </main>
    <div class="overlay"></div>
  </div>
  </div>
  <?php include 'admintguardar.php'; ?>
  <!-- partial -->
  <script src='https://unpkg.com/@popperjs/core@2'></script>
  <script src="./script.js"></script>
  <!-- JavaScript Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous">
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>

</html>
<?php
}


else
{
    header("Location: index.html");
}
?>