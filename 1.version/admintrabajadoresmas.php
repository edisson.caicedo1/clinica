<?php 
   session_start();
   $loginnombre=$_SESSION['login'];
   if($_SESSION["logueado"]==TRUE)
   {

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Clínica</title>
  <link rel='stylesheet' href='https://unpkg.com/css-pro-layout@1.1.0/dist/css/css-pro-layout.css'>
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'>
  <link rel="stylesheet" href="css/style.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.6.1.min.js"
    integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
</head>

<body>
  <!-- partial:index.partial.html -->

  <?php include ('menu.php') ?>

  <div id="overlay" class="overlay"></div>
  <div class="layout">
    <header class="header">
      <a id="btn-collapse" href="#">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <a id="btn-toggle" href="#" class="sidebar-toggler break-point-lg">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <span class="menu-title">Trabajadores</span>
    </header>
    <main class="content">
      <div>
        <div class="row">
          <div class="col-md-12">
            <!--Tabs -->
            <ul class="nav nav-tabs mt-3">
              <li class="nav-item">
                <a href="#seccion1" class="nav-link active" data-bs-toggle="tab">Datos Personales</a>
              </li>
              <li class="nav-item">
                <a href="#seccion2" class="nav-link" data-bs-toggle="tab">Formación académica</a>
              </li>
              <li class="nav-item">
                <a href="#seccion3" class="nav-link" data-bs-toggle="tab">Trabajo</a>
              </li>
              <li class="nav-item">
                <a href="#seccion4" class="nav-link" data-bs-toggle="tab">Estadísticas</a>
              </li>
            </ul>
            <!--End Tabs-->
            <!--Tabs content-->
            <div class="tab-content">
              <div class="tab-pane fade show active" id="seccion1">
                <!--Consulta a la base de datos -->
                <?php
                      include "conexion.php";
                      $id= $_GET['id'];
                      $sql="SELECT * FROM trabajadores WHERE codtrabajador=$id";
                      
                      $query = mysqli_query($mysqli, $sql);
                      while ($row = mysqli_fetch_array($query))
                  {
                    $codclinica = $row['codclinica'];
                    $foto = $row['foto'];
                    $nombres = $row['nombrest'];
                    $apellidos = $row['apellidost'];
                    $documento = $row['documento'];
                    $fecha_nacimiento = $row['fecha_nacimiento'];
                    $direccion = $row['direccion'];
                    $telefono = $row['telefono'];
                    $titulo_pregrado = $row['titulo_pregrado'];
                    $titulo_pregrado_s = $row['titulo_pregrado_s'];
                    $titulo_esp = $row['titulo_esp'];
                    $titulo_esp_s = $row['titulo_esp_s'];
                    $titulo_maestria = $row['titulo_maestria'];
                    $titulo_maestria_s = $row['titulo_maestria_s'];
                    $fecha = $row['fecha_incorporacion'];
                    $estado = $row['estado'];
                  }
                  $sql1="SELECT * FROM clinicas WHERE codclinica=$codclinica";
                  $query1 = mysqli_query($mysqli, $sql1);
                  while ($row = mysqli_fetch_array($query1))
                  { 
                    $nombreclinica = $row['nombre'];
                  }
                  ?>
                <!--Fin consulta a la base de datos -->
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                      <br>
                      <center><img class="img-circle" src="<?php echo $foto; ?>" width="100" height="100"></center>
                      <div class="card-body">
                        <h3 align="center">Datos personales</h3>
                        <table class="table table-hover">
                          <tr>
                            <td><strong>Nombres:</strong></td>
                            <td><p class="card-text"><?php echo $nombres; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Apellidos:</strong></td>
                            <td><p class="card-text"><?php echo $apellidos; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Documento:</strong></td>
                            <td><p class="card-text"><?php echo $documento; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Fecha de Nacimiento:</strong></td>
                            <td><p class="card-text"><?php echo $fecha_nacimiento; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Dirección:</strong></td>
                            <td><p class="card-text"><?php echo $direccion; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Teléfono:</strong></td>
                            <td><p class="card-text"><?php echo $telefono; ?></p></td>
                          </tr>
                        </table>
                        
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>

              <div class="tab-pane fade" id="seccion2">
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                    <br>
                      <center><img class="img-circle" src="<?php echo $foto; ?>" width="100" height="100"></center>
                      <div class="card-body">
                        <h3 align="center">Formación académica</h3>
                        <table class="table table-hover">
                          <tr>
                            <td><strong>Pregrado:</strong></td>
                            <td><p class="card-text"><?php echo $titulo_pregrado;?><br>
                                  <a href="<?php echo $titulo_pregrado_s;?>" target="_blank">Descargar soporte </a>
                                </p>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Especialización:</strong></td>
                            <td>
                            <p class="card-text"><?php echo $titulo_esp; ?><br>
                              <a href="<?php echo $titulo_esp_s;?>" target="_blank">Descargar soporte </a>
                            </p>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Maestría:</strong></td>
                            <td>
                            <p class="card-text"><?php echo $titulo_maestria; ?><br>
                              <a href="<?php echo $titulo_maestria_s;?>" target="_blank">Descargar soporte </a>
                            </p>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>

              <div class="tab-pane fade" id="seccion3">
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                    <br>
                      <center><img class="img-circle" src="<?php echo $foto; ?>" width="100" height="100"></center>
                      <div class="card-body">
                        <h3 align="center">Empresa</h3>
                        <table class="table table-hover">
                          <tr>
                            <td><strong>Clínica:</strong></td>
                            <td>
                              <p class="card-text"><?php echo $nombreclinica; ?></p>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Fecha de incorporación:</strong></td>
                            <td>
                              <p class="card-text"><?php echo $fecha; ?></p>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Estado:</strong></td>
                            <td>
                            <p class="card-text"><?php echo $estado ?></p>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>

              <div class="tab-pane fade" id="seccion4">
                <div class="row">
                  
                  <div class="col-md-12">
                    <div class="card">
                    <br>
                      <div class="card-body">
                        <h3 align="center">Estadísticas</h3>
                        
                        <p class="card-text">Pendiente</p>
                        
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <!--End Tabs content-->
          </div>
        </div>
      </div>
    </main>
    <footer class="footer">
      <small style="margin-bottom: 20px; display: inline-block">
        © 2022
      </small>
      <br />
      <div>

      </div>
    </footer>
    </main>
    <div class="overlay"></div>
  </div>
  </div>
  <?php include 'admintguardar.php'; ?>
  <!-- partial -->
  <script src='https://unpkg.com/@popperjs/core@2'></script>
  <script src="./script.js"></script>
  <!-- JavaScript Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous">
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>

</html>
<?php
}


else
{
    header("Location: index.html");
}
?>