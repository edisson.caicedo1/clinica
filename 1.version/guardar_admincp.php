<?php
require_once "conexion.php";
error_reporting(0);
if (
    isset($_POST['clinica']) && !empty($_POST['clinica']) &&
	isset($_POST['fecha']) && !empty($_POST['fecha']) &&
	isset($_POST['hora']) && !empty($_POST['hora']) &&
	isset($_POST['nombres']) && !empty($_POST['nombres'])&&
    isset($_POST['apellidos']) && !empty($_POST['apellidos'])&&
    isset($_POST['telefono']) && !empty($_POST['telefono'])&&
	isset($_POST['correo']) && !empty($_POST['correo'])&&
    /*isset($_POST['wap']) && !empty($_POST['wap'])&&
    isset($_POST['cor']) && !empty($_POST['cor'])&&
    isset($_POST['txt']) && !empty($_POST['txt'])&&
    isset($_POST['llamada']) && !empty($_POST['llamada'])&&*/
    isset($_POST['estado']) && !empty($_POST['estado'])
) 
{
    $wap = "";
    $cor = "";
    $txt = "";
    $llamada = "";

    $codcl = $_POST['clinica'];
    $fecha = $_POST['fecha'];
	$hora = $_POST['hora'];
	$nombres = $_POST['nombres'];
    $apellidos = $_POST['apellidos'];
    $telefono = $_POST['telefono'];
    $correo = $_POST['correo'];
    $wap = $_POST['wap'];
    $cor = $_POST['cor'];
    $txt = $_POST['txt'];
    $llamada = $_POST['llamada'];
    $estado = $_POST['estado'];

    if(mysqli_query($mysqli, "INSERT INTO clientespotenciales (codclinica, fecha_contacto, hora_contacto, nombresc, apellidosc, telefono, correo, wap, cor, txt, llamada, estado) VALUES ('$codcl', '$fecha', '$hora', '$nombres', '$apellidos', '$telefono', '$correo', '$wap', '$cor', '$txt', '$llamada', '$estado')"))
    {
        ?>
        <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                    <script src="sweetalert2.all.min.js"></script>
                </head>
                <body>
                <script>
                        Swal.fire({
                            title: 'Terminado',
                            text: "Datos almacenados correctamente",
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                    window.location.href='adminclientespotenciales.php';
                            }
                        })
                </script>    
                </body>
        </html>
        <?php
    }
    else
    {
        ?>
        <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                    <script src="sweetalert2.all.min.js"></script>
                </head>
                <body>
                <script>
                        Swal.fire({
                            title: 'Error',
                            text: "No fue posible tramitar tu solicitud, intenta más tarde.",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href='adminclientespotenciales.php';
                            }
                        })
                </script>    
                </body>
        </html>
        <?php
    }
}

?>