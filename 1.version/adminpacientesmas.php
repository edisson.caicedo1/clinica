<?php 
   session_start();
   $loginnombre=$_SESSION['login'];
   if($_SESSION["logueado"]==TRUE)
   {

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Clínica</title>
  <link rel='stylesheet' href='https://unpkg.com/css-pro-layout@1.1.0/dist/css/css-pro-layout.css'>
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'>
  <link rel="stylesheet" href="css/style.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.6.1.min.js"
    integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
</head>

<body>
  <!-- partial:index.partial.html -->

  <?php include ('menu.php') ?>

  <div id="overlay" class="overlay"></div>
  <div class="layout">
    <header class="header">
      <a id="btn-collapse" href="#">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <a id="btn-toggle" href="#" class="sidebar-toggler break-point-lg">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <span class="menu-title">Pacientes</span>
    </header>
    <main class="content">
      <div>
        <div class="row">
          <div class="col-md-12">
            <!--Tabs -->
            <ul class="nav nav-tabs mt-3">
              <li class="nav-item">
                <a href="#seccion1" class="nav-link active" data-bs-toggle="tab">Datos generales</a>
              </li>
              <li class="nav-item">
                <a href="#seccion2" class="nav-link" data-bs-toggle="tab">Datos médicos</a>
              </li>
              <li class="nav-item">
                <a href="#seccion3" class="nav-link" data-bs-toggle="tab">Datos Clínicos</a>
              </li>
              <li class="nav-item">
                <a href="#seccion4" class="nav-link" data-bs-toggle="tab">Historia Clínica</a>
              </li>
              <li class="nav-item">
                <a href="#seccion5" class="nav-link" data-bs-toggle="tab">Citas</a>
              </li>
            </ul>
            <!--End Tabs-->
            <!--Tabs content-->
            <div class="tab-content">
              <div class="tab-pane fade show active" id="seccion1">
                <!--Consulta a la base de datos -->
                <?php
                      include "conexion.php";
                      $id= $_GET['id'];
                      $sql="SELECT * FROM paciente WHERE codpaciente=$id";
                      
                      $query = mysqli_query($mysqli, $sql);
                      while ($row = mysqli_fetch_array($query))
                  {
                    $codclinica = $row['codclinica'];
                    $foto = $row['foto'];
                    $dni = $row['codpaciente'];
                    $nombres = $row['nombresp'];
                    $apellidos = $row['apellidosp'];
                    $correo = $row['correo'];
                    $telefono = $row['telefono'];
                    $pagopendiente = $row['pagopendiente'];
                    $etiqueta = $row['etiqueta'];
                    $estado = $row['estado'];
                    $peso = $row['peso'];
                    $altura = $row['altura'];
                    $alergias = $row['alergias'];
                    $enfermedades = $row['enfermedades'];
                  }
                  ?>
                <!--Fin consulta a la base de datos -->
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                      <br>
                      <center><img class="img-circle" src="<?php echo $foto; ?>" width="100" height="100"></center>
                      <div class="card-body">
                        <h3 align="center">Datos personales</h3>
                        <table class="table table-hover">
                          <tr>
                            <td><strong>Nombres:</strong></td>
                            <td><p class="card-text"><?php echo $nombres; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Apellidos:</strong></td>
                            <td><p class="card-text"><?php echo $apellidos; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Documento:</strong></td>
                            <td><p class="card-text"><?php echo $dni; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Correo:</strong></td>
                            <td><p class="card-text"><?php echo $correo; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Teléfono:</strong></td>
                            <td><p class="card-text"><?php echo $telefono; ?></p></td>
                          </tr>
                        </table>
                        
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>

              <div class="tab-pane fade" id="seccion2">
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                    <br>
                      <div class="card-body">
                        <h3 align="center">Datos médicos</h3>
                        <form action="actualizar_paciente_dt.php" method="POST">
                          <input id="id" name="id" class="form-control" type="hidden" value="<?php echo $id; ?>">
                        <table class="table table-hover">
                          <tr>
                            <td><strong>Nombres:</strong></td>
                            <td><p class="card-text"><?php echo $nombres; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Peso:</strong></td>
                            <td>
                              <input type="text" class="form-control" id="peso" name="peso" value="<?php echo $peso; ?>">
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Altura:</strong></td>
                            <td>
                              <input type="text" class="form-control" id="altura" name="altura" value="<?php echo $altura; ?>">
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Alergias:</strong></td>
                            <td>
                              <input type="text" class="form-control" id="alergias" name="alergias" value="<?php echo $alergias; ?>">
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Enfermedades:</strong></td>
                            <td>
                              <input type="text" class="form-control" id="enfermedades" name="enfermedades" value="<?php echo $enfermedades; ?>">
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" align="center"><button type="submit" class="btn btn-primary">Actualizar</button>  </td>
                          </tr>
                        </table>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>

              <div class="tab-pane fade" id="seccion3">
                <div class="row">
                  <br>
                <h3 align="center">Historia Clínica</h3>
                  <div class="col-md-12">
                    <div class="card">
                    <br>
                      <div class="card-body">
                          <input id="id" name="id" class="form-control" type="hidden" value="<?php echo $id; ?>">
                          <table class="table table-hover">
                            <thead>
                              <tr class="table-secondary">
                                <th scope="col">Fecha Cita</th>
                                <th scope="col">Hora Cita</th>
                                <th scope="col">Tratamiento</th>
                                <th scope="col">Quien apunta</th>
                                <th scope="col">Fecha registro</th>
                                <th scope="col">Hora registro</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Pago</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                                    include "conexion.php";
                                    $sql="SELECT * FROM tratamiento WHERE codpaciente=$id";
                                    $query = mysqli_query($mysqli, $sql);
                                    while ($row = mysqli_fetch_array($query))
                                    {?>
                              <tr>
                                <td><?php echo $row['fecha_cita']; ?></td>
                                <td><?php echo $row['hora_cita']; ?></td>
                                <td><?php echo $row['tratamiento']; ?></td>
                                <td><?php echo $row['quien_apunta']; ?></td>
                                <td><?php echo $row['fecha_registro']; ?></td>
                                <td><?php echo $row['hora_registro']; ?></td>
                                <td><?php echo $row['estado']; ?></td>
                                <td><?php echo $row['pago']; ?></td>
                              </tr>
                              <tr>
                            <?php
                            }                    
                            ?>
                            </tbody>
                          </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="tab-pane fade" id="seccion4">
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                      <div class="card-body">
                        <h3 align="center">Historia Clínica</h3>
                        <form action="actualizar_paciente_hc.php" method="POST">
                          <input id="id" name="id" class="form-control" type="hidden" value="<?php echo $id; ?>">
                        <table class="table table-hover">
                          <tr>
                            <td><strong>Nombres:</strong></td>
                            <td><p class="card-text"><?php echo $nombres; ?></p></td>
                          </tr>
                          <tr>
                            <td><strong>Fecha cita:</strong></td>
                            <td>
                              <input type="date" class="form-control" id="fecha_cita" name="fecha_cita" required>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Fecha hora:</strong></td>
                            <td>
                              <input type="time" class="form-control" id="hora_cita" name="hora_cita" required>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Tratamiento:</strong></td>
                            <td>
                              <input type="text" class="form-control" id="tratamiento" name="tratamiento" required>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Quien apunta:</strong></td>
                            <td>
                              <input type="text" class="form-control" id="quien_apunta" name="quien_apunta" required>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Fecha Registro:</strong></td>
                            <td>
                              <input type="date" class="form-control" id="fecha_registro" name="fecha_registro" required>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Hora Registro:</strong></td>
                            <td>
                              <input type="time" class="form-control" id="hora_registro" name="hora_registro" required>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Estado:</strong></td>
                            <td>
                            <select class="form-select" aria-label="Default select example" name="estado">
                              <option selected></option>
                              <option value="Completada">Completada</option>
                              <option value="En curso">En curso</option>
                              <option value="No se realizo">No se realizo</option>
                            </select>
                            </td>
                          </tr>
                          <tr>
                            <td><strong>Pago:</strong></td>
                            <td>
                            <select class="form-select" aria-label="Default select example" name="pago">
                              <option selected></option>
                              <option value="Pagó">Pagó</option>
                              <option value="No pagó">No pagó</option>
                            </select>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2" align="center"><button type="submit" class="btn btn-primary">Actualizar</button>  </td>
                          </tr>
                        </table>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>

              <div class="tab-pane fade" id="seccion5">
                <div class="row">
                  <div class="col-md-4">
                    <div class="card">
                      <div class="card-body">
                        <h3 align="center">Agendar Citas</h3>
                        <form action="actualizar_paciente_citas.php" method="POST">
                          <input id="id" name="id" class="form-control" type="hidden" value="<?php echo $id; ?>">
                          <table class="table table-hover">
                            <tr>
                              <td><strong>Nombres:</strong></td>
                              <td><p class="card-text"><?php echo $nombres; ?></p></td>
                            </tr>
                            <tr>
                              <td><strong>Fecha cita:</strong></td>
                              <td>
                                <input type="date" class="form-control" id="fecha_cita" name="fecha_cita" required>
                              </td>
                            </tr>
                            <tr>
                              <td><strong>Hora cita:</strong></td>
                              <td>
                                <input type="time" class="form-control" id="hora_cita" name="hora_cita" required>
                              </td>
                            </tr>
                            
                            <tr>
                              <td colspan="2" align="center"><button type="submit" class="btn btn-primary">Agendar</button>  </td>
                            </tr>
                          </table>
                        </form>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="card">
                      <div class="card-body">
                        <h3 align="center">Citas</h3>
                        <table class="table table-hover">
                          <thead>
                            <tr class="table-secondary">
                              <th scope="col">Fecha Cita</th>
                              <th scope="col">Hora Cita</th>
                              <th scope="col" >Estado</th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php
                              include "conexion.php";
                              
                              $sql="SELECT * FROM citas";
                              $query = mysqli_query($mysqli, $sql);
                              while ($row = mysqli_fetch_array($query))
                              {?>
                            <tr>
                              <td><?php echo $row['fecha_cita']; ?></td>
                              <td><?php echo $row['hora_cita']; ?></td>
                              <td><?php echo $row['estado']; ?></td>
                            </tr>
                            <tr>
                              <?php
                                }
                              ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <!--End Tabs content-->
          </div>
        </div>
      </div>
    </main>
    <footer class="footer">
      <small style="margin-bottom: 20px; display: inline-block">
        © 2022
      </small>
      <br />
      <div>

      </div>
    </footer>
    </main>
    <div class="overlay"></div>
  </div>
  </div>
  <?php include 'admintguardar.php'; ?>
  <!-- partial -->
  <script src='https://unpkg.com/@popperjs/core@2'></script>
  <script src="./script.js"></script>
  <!-- JavaScript Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous">
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>

</html>
<?php
}


else
{
    header("Location: index.html");
}
?>