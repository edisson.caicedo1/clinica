<?php
require_once "conexion.php";
if (
    isset($_POST['id']) && !empty($_POST['id']) &&
    isset($_POST['fecha_cita']) && !empty($_POST['fecha_cita']) &&
    isset($_POST['hora_cita']) && !empty($_POST['hora_cita']) &&
    isset($_POST['tratamiento']) && !empty($_POST['tratamiento'])&&
    isset($_POST['quien_apunta']) && !empty($_POST['quien_apunta'])&&
    isset($_POST['fecha_registro']) && !empty($_POST['fecha_registro'])&&
    isset($_POST['hora_registro']) && !empty($_POST['hora_registro'])&&
    isset($_POST['estado']) && !empty($_POST['estado'])&&
    isset($_POST['pago']) && !empty($_POST['pago'])
)
{
    $id = $_POST['id'];
    $fecha_cita = $_POST['fecha_cita'];
    $hora_cita = $_POST['hora_cita'];
    $tratamiento = $_POST['tratamiento'];
    $quien_apunta = $_POST['quien_apunta'];
    $fecha_registro = $_POST['fecha_registro'];
    $hora_registro = $_POST['hora_registro'];
    $estado = $_POST['estado'];
    $pago = $_POST['pago'];

    $sql= "INSERT INTO tratamiento (codpaciente, fecha_cita, hora_cita, tratamiento, quien_apunta, fecha_registro, hora_registro, estado, pago) VALUES ('$id', '$fecha_cita', '$hora_cita', '$tratamiento', '$quien_apunta','$fecha_registro','$hora_registro','$estado','$pago')";

    if(mysqli_query($mysqli, $sql))
    {
        ?>
        <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                    <script src="sweetalert2.all.min.js"></script>
                </head>
                <body>
                <script>
                        Swal.fire({
                            title: 'Terminado',
                            text: "Datos almacenados correctamente",
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                    window.location.href='adminpacientes.php';
                            }
                        })
                </script>    
                </body>
        </html>
        <?php
    }
    else
    {
        ?>
        <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                    <script src="sweetalert2.all.min.js"></script>
                </head>
                <body>
                <script>
                        Swal.fire({
                            title: 'Error',
                            text: "No fue posible tramitar tu solicitud, intenta más tarde.",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href='adminpacientes.php';
                            }
                        })
                </script>    
                </body>
        </html>
        <?php
    }
}   
?>