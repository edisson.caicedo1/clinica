<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tiiz Ortodoncia invisible</title>

    <link rel="stylesheet" href="css/estilos.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'>
    <script src="https://kit.fontawesome.com/41bcea2ae3.js" crossorigin="anonymous"></script>
    <link rel="icon" type="image/x-icon" href="img/img_logo_tizz.ico"/>
    
</head>
<body id="body">
    
    <header>
        <table width="80%">
            <tr>
                <td>
                    <div class="icon__menu">
                        <i class="fas fa-bars" id="btn_open"></i>
                    </div>
                    
                </td>
                <td>
                <h4>Trabajadores</h4>
                </td>
                <td align='right'>
                    <a href=""><img src="img/perfil.png" alt=""></a>
                    <a href="salir.php"><img src="img/salir.png" alt=""></a>
                </td>
            </tr>
        </table>
        
    </header>
    <div class="menu__side" id="menu_side">

        <div class="name__page">
            <i><img src="img/img_logo_tizz@2x.png" width="100" height=""></i>
        </div>

        <div class="options__menu">	

            <a href="#" class="selected">
                <div class="option">
                    <i class="fas fa-home" title="Inicio"></i>
                    <h4>Inicio</h4>
                </div>
            </a>

            <a href="admintrabajadores.php">
                <div class="option">
                    <i class="ri-contacts-book-line" title="Trabajadores"></i>
                    <h4>Trabajadores</h4>
                </div>
            </a>
            
            <a href="adminclinicas.php">
                <div class="option">
                    <i class="ri-hospital-line" title="Clinicas"></i>
                    <h4>Clínicas</h4>
                </div>
            </a>

            <a href="adminclientespotenciales.php">
                <div class="option">
                    <i class="ri-folder-user-fill" title="Clientes potenciales"></i>
                    <h4>Clientes potenciales</h4>
                </div>
            </a>

            <a href="adminpacientes.php">
                <div class="option">
                    <i class="ri-creative-commons-by-line" title="Pacientes"></i>
                    <h4>Pacientes</h4>
                </div>
            </a>

            <a href="adminagenda.php">
                <div class="option">
                    <i class="ri-article-line" title="Agenda"></i>
                    <h4>Agenda</h4>
                </div>
            </a>

            <a href="#">
                <div class="option">
                    <i class="ri-profile-fill" title="Parametrización"></i>
                    <h4>Parametrización</h4>
                </div>
            </a>

        </div>

    </div>
    <div class="contenido">
        <p align="right" >
            <button type="button" class="agregar">Agregar</button>
        </p>
    </div>
    <div class="contenedor">
        <table>
            <thead>
              <tr>
                <th>Foto</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Documento</th>
                <th>Estado</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                <!--Para la conexión a la base de datos-->
                <tr>
                <td><img class="img-circle" src="<?php echo $row['foto']; ?>" width="50" height="50"></td>
                <td><?php echo $row['nombrest']; ?></td>
                <td><?php echo $row['apellidost']; ?></td>
                <td><?php echo $row['documento']; ?></td>
                <td><?php echo $row['estado']; ?></td>
                <td><a href="admintrabajadoresmas.php?id=<?php echo $row['codtrabajador']?>" class="btn btn-info">Más</a>
                <a href="admintrabajadoresedit.php?id=<?php echo $row['codtrabajador']?>" class="btn btn-primary">Editar</a>
                </td>
                </tr>
        </table>
    </div>


    <script src="js/script.js"></script>
</body>
</html>