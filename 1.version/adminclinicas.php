<?php 
   session_start();
   $loginnombre=$_SESSION['login'];
   if($_SESSION["logueado"]==TRUE)
   {
?>

<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Clínica</title>
  <link rel='stylesheet' href='https://unpkg.com/css-pro-layout@1.1.0/dist/css/css-pro-layout.css'>
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'><link rel="stylesheet" href="css/style.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
</head>
<body>
<!-- partial:index.partial.html -->

<?php include ('menu.php') ?>

  <div id="overlay" class="overlay"></div>
  <div class="layout">
    <header class="header">
      <a id="btn-collapse" href="#">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <a id="btn-toggle" href="#" class="sidebar-toggler break-point-lg">
        <i class="ri-menu-line ri-xl"></i>
        </a>
        <span class="menu-title">Clínicas</span>
    </header>
    <main class="content">
      <div>
        <div class="row">
          <div class="col-md-6">
            
          </div>
          <div class="col-md-6">
            <p align="right" >
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#guardarCdmodal" data-bs-whatever="@mdo"></span> Agregar</button>
            </p>
          </div>
        </div>
          <br>
        <div>
          <table class="table table-hover">
            <thead>
              <tr class="table-secondary">
                <th scope="col">Clínica</th>
                <th scope="col">Dirección</th>
                <th scope="col" >Gabinetes</th>
                <th scope="col">Estado</th>
              </tr>
            </thead>
            <tbody>
              <?php
                    include "conexion.php";
                    
                    $sql="SELECT * FROM clinicas";
                    $query = mysqli_query($mysqli, $sql);
                    while ($row = mysqli_fetch_array($query))
                    {?>
              <tr>
                <td><?php echo $row['nombre']; ?></td>
                <td><?php echo $row['direccion']; ?></td>
                <td>
                  <a href="adminclinicasgabinente.php?consulta=<?php echo $row['codclinica'];?>"> <button class="btn btn-info"> Consultar</button> </a> 
                </td>
                <td><?php echo $row['estado']; ?></td>
              </tr>
              <tr>
            <?php
            }
            ?>
            </tbody>
          </table>
        </div>
      </div>
      
      <footer class="footer">
        <small style="margin-bottom: 20px; display: inline-block">
          © 2022
        </small>
        <br />
        <div>
          
        </div>
      </footer>
    </main>
    <div class="overlay"></div>
  </div>
</div>


<!-- partial -->
<?php include 'admincguardar.php'; ?>
<!-- partial -->
  <script src='https://unpkg.com/@popperjs/core@2'></script><script  src="./script.js"></script>
  <!-- JavaScript Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>
</html>
<?php
}

else
{
    header("Location: index.html");
}
?>