<?php 
   session_start();
   $loginnombre=$_SESSION['login'];
   if($_SESSION["logueado"]==TRUE)
   {

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Clínica</title>
  <link rel='stylesheet' href='https://unpkg.com/css-pro-layout@1.1.0/dist/css/css-pro-layout.css'>
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'>
  <link rel="stylesheet" href="css/style.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.6.1.min.js"
    integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
</head>

<body>
  <!-- partial:index.partial.html -->

  <?php include ('menu.php') ?>

  <div id="overlay" class="overlay"></div>
  <div class="layout">
    <header class="header">
      <a id="btn-collapse" href="#">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <a id="btn-toggle" href="#" class="sidebar-toggler break-point-lg">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <span class="menu-title">Trabajadores</span>
    </header>
    <main class="content">
      <div>
        <div class="row">
          <div class="col-md-12">
            <!--Tabs -->
            <ul class="nav nav-tabs mt-3">
              <li class="nav-item">
                <a href="#seccion1" class="nav-link active" data-bs-toggle="tab">Datos Personales</a>
              </li>
              <li class="nav-item">
                <a href="#seccion2" class="nav-link" data-bs-toggle="tab">Formación académica</a>
              </li>
              <li class="nav-item">
                <a href="#seccion3" class="nav-link" data-bs-toggle="tab">Trabajo</a>
              </li>
            </ul>
            <!--End Tabs-->

            <!--Tabs content-->

            <!--Formulario de actualización -->
            <form action="actualizar_admint.php" method="POST" enctype="multipart/form-data">
            <div class="tab-content">
              <div class="tab-pane fade show active" id="seccion1">
                <!--Consulta a la base de datos -->
                <?php
                      include "conexion.php";
                      $id= $_GET['id'];
                      $sql="SELECT * FROM trabajadores WHERE codtrabajador=$id";
                      $query = mysqli_query($mysqli, $sql);
                      while ($row = mysqli_fetch_array($query))
                  {
                    $codclinica = $row['codclinica'];
                    $foto = $row['foto'];
                    $nombres = $row['nombrest'];
                    $apellidos = $row['apellidost'];
                    $documento = $row['documento'];
                    $fecha_nacimiento = $row['fecha_nacimiento'];
                    $direccion = $row['direccion'];
                    $telefono = $row['telefono'];
                    $email = $row['email'];
                    $titulo_pregrado = $row['titulo_pregrado'];
                    $titulo_pregrados = $row['titulo_pregrado_s'];
                    $titulo_esp = $row['titulo_esp'];
                    $titulo_esps = $row['titulo_esp_s'];
                    $titulo_maestria = $row['titulo_maestria'];
                    $fecha = $row['fecha_incorporacion'];
                    $titulo_maestrias = $row['titulo_maestria_s'];
                    $estado = $row['estado'];
                    $rol = $row['rol'];
                  }
                  
                  $sql1="SELECT * FROM clinicas WHERE codclinica=$codclinica";
                  $query1 = mysqli_query($mysqli, $sql1);
                  while ($row = mysqli_fetch_array($query1))
                  { 
                    $nombreclinica = $row['nombre'];
                  }
                  ?>
                  
                <!--Fin consulta a la base de datos -->

                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <!--Sección uno Datos personales -->
                    <div class="card">
                      <center><img class="img-circle" src="<?php echo $foto; ?>" width="100" height="100"></center>
                      <div class="card-body">
                        <h3 align="center">Datos personales</h3>
                        <input id="cod" name="cod" class="form-control" type="hidden" value="<?php echo $id; ?>">
                        <input type="file" class="form-control" id="foto" name="foto" accept="image/png, .jpeg, .jpg">
                        <label for="recipient-name" class="col-form-label"><strong>Nombres</strong></label>
                        <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombres; ?>">
                        <label for="recipient-name" class="col-form-label"><strong>Apellidos</strong></label>
                        <input type="text" class="form-control" id="apellidos" name="apellidos" value="<?php echo $apellidos; ?>">
                        <label for="recipient-name" class="col-form-label"><strong>Fecha de Nacimiento</strong></label>
                        <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" value="<?php echo $fecha_nacimiento; ?>">
                        <label for="recipient-name" class="col-form-label"><strong>Dirección</strong></label>
                        <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $direccion; ?>">
                        <label for="recipient-name" class="col-form-label"><strong>Teléfono</strong></label>
                        <input type="number" class="form-control" id="telefono" name="telefono" value="<?php echo $telefono; ?>">
                        <label for="recipient-name" class="col-form-label"><strong>E-mail</strong></label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>">

                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>
                  <!--Sección dos Formación acádemica -->
              <div class="tab-pane fade" id="seccion2">
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                      <center><img class="img-circle" src="<?php echo $foto; ?>" width="100" height="100"></center>
                      <div class="card-body">
                        <h3 align="center">Formación académica</h3>
                        <label for="recipient-name" class="col-form-label"><strong>Título pregrado</strong></label>
                        <input type="text" class="form-control" id="tpregrado" name="tpregrado" value="<?php echo $titulo_pregrado; ?>">
                        <label for="recipient-name" class="col-form-label"><strong>Título Especialización</strong></label>
                        <input type="text" class="form-control" id="tesp" name="tesp" value="<?php echo $titulo_esp; ?>">
                        <label for="recipient-name" class="col-form-label"><strong>Título Maestria</strong></label>
                        <input type="text" class="form-control" id="tmaestria" name="tmaestria" value="<?php echo $titulo_maestria; ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>
              </div>
              
              <!--Sección tres Datos empresa -->
              
              <div class="tab-pane fade" id="seccion3">
                <div class="row">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <div class="card">
                      <center><img class="img-circle" src="<?php echo $foto; ?>" width="100" height="100"></center>
                      <div class="card-body">
                        <h3 align="center">Empresa</h3>
                        <input type="text" class="form-control" id="nombreclinica" name="nombreclinica" value="<?php echo $nombreclinica; ?>" READONLY><br>
                        <input type="date" class="form-control" id="fecha" name="fecha" value="<?php echo $fecha; ?>" READONLY><br>
                        <select class="form-select" aria-label="Default select example" name="estado">
                          <option selected></option>
                          <option value="Activo">Activo</option>
                          <option value="Inactivo">Inactivo</option>
                        </select>
                        <br>
                        <button type="submit" class="btn btn-primary">Actualizar</button>  
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3"></div>
                </div>

              </div>
            </div>
            </form>
            <!--Formulario de actualización -->
            <!--End Tabs content-->
          </div>
        </div>
      </div>
    </main>
    <footer class="footer">
      <small style="margin-bottom: 20px; display: inline-block">
        © 2022
      </small>
      <br />
      <div>

      </div>
    </footer>
    </main>
    <div class="overlay"></div>
  </div>
  </div>
  <?php include 'admintguardar.php'; ?>
  <!-- partial -->
  <script src='https://unpkg.com/@popperjs/core@2'></script>
  <script src="./script.js"></script>
  <!-- JavaScript Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous">
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>

</html>
<?php
}


else
{
    header("Location: index.html");
}
?>