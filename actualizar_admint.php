<?php
require_once "conexion.php";
if (
    isset($_POST['cod']) && !empty($_POST['cod']) &&
    isset($_POST['nombre']) && !empty($_POST['nombre']) &&
    isset($_POST['apellidos']) && !empty($_POST['apellidos']) &&
    isset($_POST['fecha_nacimiento']) && !empty($_POST['fecha_nacimiento'])&&
    isset($_POST['direccion']) && !empty($_POST['direccion'])&&
    isset($_POST['telefono']) && !empty($_POST['telefono'])&&
    isset($_POST['email']) && !empty($_POST['email'])&&
    isset($_POST['tpregrado']) && !empty($_POST['tpregrado'])&&
    isset($_POST['tesp']) && !empty($_POST['tesp'])&&
    isset($_POST['tmaestria']) && !empty($_POST['tmaestria'])&&
    isset($_POST['estado']) && !empty($_POST['estado'])&&
    isset($_POST['fecha']) && !empty($_POST['fecha'])
)
{
    $cod = $_POST['cod'];
    /*$clinica = $_POST['clinica'];*/
    $nombres = $_POST['nombre'];
    $apellidos = $_POST['apellidos'];
    /*$documento = $_POST['documento'];*/
    $fecha_nacimiento = $_POST['fecha_nacimiento'];
    $direccion = $_POST['direccion'];
    $telefono = $_POST['telefono'];
    $email = $_POST['email'];
    $titulo_pregrado = $_POST['tpregrado'];
    $titulo_esp = $_POST['tesp'];
    $titulo_maestria = $_POST['tmaestria'];
    $estado = $_POST['estado'];
    $fecha = $_POST['fecha'];

    $sql= "UPDATE trabajadores SET nombrest='$nombres', apellidost='$apellidos', fecha_nacimiento='$fecha_nacimiento', direccion='$direccion', telefono='$telefono', email='$email', titulo_pregrado='$titulo_pregrado', titulo_esp='$titulo_esp', titulo_maestria='$titulo_maestria', estado='$estado', fecha_incorporacion='$fecha' WHERE codtrabajador='$cod'";

    if(mysqli_query($mysqli, $sql))
    {
        ?>
        <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                    <script src="sweetalert2.all.min.js"></script>
                </head>
                <body>
                <script>
                        Swal.fire({
                            title: 'Terminado',
                            text: "Datos almacenados correctamente",
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                    window.location.href='admintrabajadores.php';
                            }
                        })
                </script>    
                </body>
        </html>
        <?php
    }
    else
    {
        ?>
        <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                    <script src="sweetalert2.all.min.js"></script>
                </head>
                <body>
                <script>
                        Swal.fire({
                            title: 'Error',
                            text: "No fue posible tramitar tu solicitud, intenta más tarde.",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                    window.location.href='admintrabajadores.php';
                            }
                        })
                </script>    
                </body>
        </html>
        <?php
    }
}     
?>