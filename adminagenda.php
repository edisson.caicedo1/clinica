<?php 
   session_start();
   $loginnombre=$_SESSION['login'];
   if($_SESSION["logueado"]==TRUE)
   {

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Clínica</title>
  <link rel='stylesheet' href='https://unpkg.com/css-pro-layout@1.1.0/dist/css/css-pro-layout.css'>
  <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css'>
  <link rel="stylesheet" href="css/style.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.6.1.min.js"
    integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
</head>

<body>
  <!-- partial:index.partial.html -->

  <?php include ('menu.php') ?>

  <div id="overlay" class="overlay"></div>
  <div class="layout">
    <header class="header">
      <a id="btn-collapse" href="#">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <a id="btn-toggle" href="#" class="sidebar-toggler break-point-lg">
        <i class="ri-menu-line ri-xl"></i>
      </a>
      <span class="menu-title">Agenda</span>
    </header>
    <main class="content">
      <div>
        <div class="row">
          <div class="col-md-12">
            <!--Tabs -->
            <ul class="nav nav-tabs mt-3">
              <li class="nav-item">
                <a href="#seccion1" class="nav-link active" data-bs-toggle="tab">Agenda</a>
              </li>
              <li class="nav-item">
                <a href="#seccion2" class="nav-link" data-bs-toggle="tab">Actualizar agenda</a>
              </li>
            </ul>
            <!--End Tabs-->
            <!--Tabs content-->
            <div class="tab-content">
              <div class="tab-pane fade show active" id="seccion1">
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8">
                    <div class="card">
                      <div class="card-body">
                        <h3 align="center">Agenda</h3>
                        <table class="table table-hover">
                          <thead>
                            <tr class="table-secondary">
                              <th scope="col">Fecha</th>
                              <th scope="col">Hora</th>
                              <th scope="col" colspan="2" align="center">Trabajador</th>
                              <th scope="col" colspan="2" align="center">Paciente</th>
                              <th scope="col">Estado</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                                  include "conexion.php";
                                  $sql="SELECT paciente.codpaciente, paciente.nombresp, paciente.apellidosp, citas.fecha_cita, citas.hora_cita, citas.estado, trabajadores.nombrest, trabajadores.apellidost FROM paciente INNER JOIN CITAS ON paciente.codpaciente = citas.codpaciente INNER JOIN trabajadores ON citas.codtrabajador = trabajadores.codtrabajador";
                                  $query = mysqli_query($mysqli, $sql);
                                  while ($row = mysqli_fetch_array($query))
                                  {?>
                            <tr>
                              <td><?php echo $row['fecha_cita']; ?></td>
                              <td><?php echo $row['hora_cita']; ?></td>
                              <td><?php echo $row['nombrest']; ?></td>
                              <td><?php echo $row['apellidost']; ?></td>
                              <td><?php echo $row['nombresp']; ?></td>
                              <td><?php echo $row['apellidosp']; ?></td>
                              <td><?php echo $row['estado']; ?></td>
                              <td></td>
                            </tr>
                            <tr>
                          <?php
                          }                    
                          ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>

              <div class="tab-pane fade" id="seccion2">
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8">
                    <div class="card">
                      <div class="card-body">
                        <h3 align="center">Agenda</h3>
                        <table class="table table-hover">
                          <thead>
                            <tr class="table-secondary">
                              <th scope="col">Fecha</th>
                              <th scope="col">Hora</th>
                              <th scope="col" colspan="2" align="center">Trabajador</th>
                              <th scope="col" colspan="2" align="center">Paciente</th>
                              <th scope="col">Estado</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                                  include "conexion.php";
                                  $sql="SELECT paciente.codpaciente, paciente.nombresp, paciente.apellidosp, citas.fecha_cita, citas.hora_cita, citas.estado, trabajadores.nombrest, trabajadores.apellidost FROM paciente INNER JOIN CITAS ON paciente.codpaciente = citas.codpaciente INNER JOIN trabajadores ON citas.codtrabajador = trabajadores.codtrabajador";
                                  $query = mysqli_query($mysqli, $sql);
                                  while ($row = mysqli_fetch_array($query))
                                  {?>
                            <tr>
                              <td><?php echo $row['fecha_cita']; ?></td>
                              <td><?php echo $row['hora_cita']; ?></td>
                              <td><?php echo $row['nombrest']; ?></td>
                              <td><?php echo $row['apellidost']; ?></td>
                              <td><?php echo $row['nombresp']; ?></td>
                              <td><?php echo $row['apellidosp']; ?></td>
                              <td><?php echo $row['estado']; ?></td>
                              <td></td>
                            </tr>
                            <tr>
                          <?php
                          }                    
                          ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>



            </div>
            <!--End Tabs content-->
          </div>
        </div>
      </div>
    </main>
    <footer class="footer">
      <small style="margin-bottom: 20px; display: inline-block">
        © 2022
      </small>
      <br />
      <div>

      </div>
    </footer>
    </main>
    <div class="overlay"></div>
  </div>
  </div>
  <?php include 'admintguardar.php'; ?>
  <!-- partial -->
  <script src='https://unpkg.com/@popperjs/core@2'></script>
  <script src="./script.js"></script>
  <!-- JavaScript Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous">
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</body>

</html>
<?php
}


else
{
    header("Location: index.html");
}
?>