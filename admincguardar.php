<!--Dat toogle Añadir -->

<div class="modal fade" id="guardarCdmodal" tabindex="-1" aria-labelledby="guardarAdmodal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Clínica</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="guardar_adminc.php" method="POST" >
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Clínica</label>
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre Clínica" required>  
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Dirección</label>
            <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" required>
          </div>
          <div>
            <label for="recipient-name" class="col-form-label">Estado</label>
            <select class="form-select" aria-label="Default select example" name="estado" required>
              <option selected></option>
              <option value="Activo">Activo</option>
              <option value="Inactivo">Inactivo</option>
            </select>
          </div>
          <br>
            <button type="submit" class="btn btn-primary">Guardar</button>  
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        </form>
      </div>
    </div>
  </div>
</div>