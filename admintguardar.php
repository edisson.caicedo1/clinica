<!--Extraer datos bd -->

<?php
  include "conexion.php";
  $sql="SELECT * FROM clinicas";
  $query = mysqli_query($mysqli, $sql);
?>

<!--Dat toogle Añadir -->

<div class="modal fade" id="guardarAdmodal" tabindex="-1" aria-labelledby="guardarAdmodal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Trabajador</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="guardar_admint.php" method="POST" enctype="multipart/form-data">
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Clínica</label>
            <select class="form-select" aria-label="Default select example" name="clinica" required>
            <option selected></option>
              <?php
              while ($row = mysqli_fetch_array($query))
              {
              ?>
              <option value="<?php echo $row['codclinica']; ?>"><?php echo $row['nombre']; ?></option>
              <?php
              }
              ?>
            </select>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Rol</label>
            <select class="form-select" aria-label="Default select example" name="rol" required>
              <option selected></option>
              <option value="Directores">Directores</option>
              <option value="Administrador">Administrador</option>
              <option value="Recepción">Recepción</option>
            </select>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Foto</label>
            <input type="file" class="form-control" id="foto" name="foto" accept="image/png, .jpeg, .jpg" required>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Nombres</label>
            <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres" required>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Apellidos</label>
            <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" required>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Documento</label>
            <input type="number" class="form-control" id="documento" name="documento" placeholder="Documento" required>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Fecha Nacimiento</label>
            <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" required>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Dirección</label>
            <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección" required> 
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Teléfono</label>
            <input type="number" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" required>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">E-mail</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" required>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Título pregrado</label>
            <input type="text" class="form-control" id="titulo_pregrado" name="titulo_pregrado" placeholder="Título pregrado" required>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Soporte Título</label>
            <input type="file" class="form-control" id="tpregrado" name="tpregrado" accept="application/pdf" required>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Título especialización</label>
            <input type="text" class="form-control" id="titulo_esp" name="titulo_esp" placeholder="Título especialización">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Soporte Título</label>
            <input type="file" class="form-control" id="tespe" name="tespe" accept="application/pdf">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Título maestría</label>
            <input type="text" class="form-control" id="titulo_maestria" name="titulo_maestria" placeholder="Título maestría">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Soporte Título</label>
            <input type="file" class="form-control" id="tmaestria" name="tmaestria" accept="application/pdf">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Estado</label>
            <select class="form-select" aria-label="Default select example" name="estado" required>
              <option selected></option>
              <option value="Activo">Activo</option>
              <option value="Inactivo">Inactivo</option>
            </select>
           </div>
           <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Fecha de inscripción</label>
            <input type="date" class="form-control" id="fecha" name="fecha" required>
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>  
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        </form>
      </div>
    </div>
  </div>
</div>