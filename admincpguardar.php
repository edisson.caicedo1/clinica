<!--Extraer datos bd -->

<?php
  include "conexion.php";
  $sql="SELECT * FROM clinicas";
  $query = mysqli_query($mysqli, $sql);
?>

<!--Dat toogle Añadir -->

<div class="modal fade" id="guardarCPdmodal" tabindex="-1" aria-labelledby="guardarCPdmodal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Clientes Potenciales</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="guardar_admincp.php" method="POST" >
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Clínica</label>
            <select class="form-select" aria-label="Default select example" name="clinica" required>
            <option selected></option>
              <?php
              while ($row = mysqli_fetch_array($query))
              {
              ?>
              <option value="<?php echo $row['codclinica']; ?>"><?php echo $row['nombre']; ?></option>
              
              <?php
              }
              ?>
            </select>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Fecha Contácto</label>
            <input type="date" class="form-control" id="fecha" name="fecha" placeholder="fecha">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Hora</label>
            <input type="time" class="form-control" id="hora" name="hora" placeholder="Hora">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Nombres</label>
            <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Apellidos</label>
            <input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Teléfono</label>
            <input type="number" class="form-control" id="telefono" name="telefono" placeholder="Teléfono">
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Correo</label>
            <input type="email" class="form-control" id="correo" name="correo" placeholder="E-Mail">
          </div>
          <label for="recipient-name" class="col-form-label">Forma de contacto</label>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="Si" id="wap" name="wap">
            <label class="form-check-label" for="flexCheckChecked">Whatsapp</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="Si" id="cor" name="cor">
            <label class="form-check-label" for="flexCheckChecked">Correo</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="Si" id="txt" name="txt">
            <label class="form-check-label" for="flexCheckChecked">Mensaje de texto</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" value="Si" id="llamada" name="llamada">
            <label class="form-check-label" for="flexCheckChecked">Llamada telefónica</label>
          </div>
          <div>
            <label for="recipient-name" class="col-form-label">Estado</label>
            <select class="form-select" aria-label="Default select example" name="estado">
              <option selected></option>
              <option value="Citado">Citado</option>
              <option value="Cita pendiente de cambio">Cita pendiente de cambio</option>
              <option value="Volver a llamar">Volver a llamar</option>
            </select>
          </div>
          <br>
            <button type="submit" class="btn btn-primary">Guardar</button>  
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        </form>
      </div>
    </div>
  </div>
</div>