<div class="layout has-sidebar fixed-sidebar fixed-header">
  <aside id="sidebar" class="sidebar break-point-lg has-bg-image">
    <div class="image-wrapper">
      
    </div>
    <div class="sidebar-layout">
      <div class="sidebar-header">
        <span style="text-transform: uppercase; font-size: 15px; letter-spacing: 3px; font-weight: bold;">Clinica</span>
      </div>
      <div class="sidebar-content">
        <nav class="menu open-current-submenu">
          <ul>
            <li class="menu-item">
              <a href="admintrabajadores.php">
                <span class="menu-icon">
                  <i class="ri-contacts-book-line"></i>
                </span>
                <span class="menu-title">Trabajadores</span>
              </a>
            </li>
            <li class="menu-item">
              <a href="adminclinicas.php">
                <span class="menu-icon">
                  <i class="ri-hospital-line"></i>
                </span>
                <span class="menu-title">Clínicas</span>
              </a>
            </li>
            <li class="menu-item">
              <a href="adminclientespotenciales.php">
                <span class="menu-icon">
                  <i class="ri-folder-user-fill"></i>
                </span>
                <span class="menu-title">Clientes potenciales</span>
              </a>
            </li>
            <li class="menu-item">
              <a href="adminpacientes.php">
                <span class="menu-icon">
                  <i class="ri-creative-commons-by-line"></i>
                </span>
                <span class="menu-title">Pacientes</span>
              </a>
            </li>
            <li class="menu-item">
              <a href="adminagenda.php">
                <span class="menu-icon">
                  <i class="ri-article-line"></i>
                </span>
                <span class="menu-title">Agenda</span>
              </a>
            </li>
            <li class="menu-item">
              <a href="#">
                <span class="menu-icon">
                  <i class="ri-profile-fill"></i>
                </span>
                <span class="menu-title">Parametrización</span>
              </a>
            </li>
            <li class="menu-item">
              <a href="salir.php">
                <span class="menu-icon">
                  <i class="ri-reply-fill"></i>
                </span>
                <span class="menu-title">Salir</span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
      <div class="sidebar-footer"><span>Derechos reservados.</span></div>
    </div>
  </aside>