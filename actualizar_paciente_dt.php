<?php
require_once "conexion.php";
if (
    isset($_POST['id']) && !empty($_POST['id']) &&
    isset($_POST['peso']) && !empty($_POST['peso']) &&
    isset($_POST['altura']) && !empty($_POST['altura']) &&
    isset($_POST['alergias']) && !empty($_POST['alergias'])&&
    isset($_POST['enfermedades']) && !empty($_POST['enfermedades'])
)
{
    $id = $_POST['id'];
    $peso = $_POST['peso'];
    $altura = $_POST['altura'];
    $alergias = $_POST['alergias'];
    $enfermedades = $_POST['enfermedades'];

    $sql= "UPDATE paciente SET peso='$peso', altura='$altura', alergias='$alergias', enfermedades='$enfermedades'WHERE codpaciente='$id'";

    if(mysqli_query($mysqli, $sql))
    {
        ?>
        <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                    <script src="sweetalert2.all.min.js"></script>
                </head>
                <body>
                <script>
                        Swal.fire({
                            title: 'Terminado',
                            text: "Datos almacenados correctamente",
                            icon: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                    window.location.href='adminpacientes.php';
                            }
                        })
                </script>    
                </body>
        </html>
        <?php
    }
    else
    {
        ?>
        <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
                    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
                    <script src="sweetalert2.all.min.js"></script>
                </head>
                <body>
                <script>
                        Swal.fire({
                            title: 'Error',
                            text: "No fue posible tramitar tu solicitud, intenta más tarde.",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                            }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href='adminpacientes.php';
                            }
                        })
                </script>    
                </body>
        </html>
        <?php
    }
}     
?>